# include <stdio.h>
# include <stdlib.h>

// The program takes input from the standard in
// and shifts each alphabetic characters by 13
// if a is given as input, n is produced as an 
// output
//
// (c) 2013 Diwas Timilsina

int main(int argc, char **argv)
{
  // input char 
  char input;  

  // while it is not end of file, and the input is an alphabet 
  // then move the alphabet by 13. if the input is not an alphabet
  // then just print the character
  while ((input = getchar())!=EOF){  
    if (input >= 'A' && input <= 'Z'){
      putchar('A' + (input + 13 - 'A') %26);
    } else if (input >= 'a' && input <= 'z'){
      putchar('a' + (input + 13 - 'a') %26); 
    } else{
      putchar(input);
    }
  }
  return 0;
}
