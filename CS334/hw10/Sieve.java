import java.io.*;

/**
 * This Sieve class reads input from the terminal and
 * inserts the character into a buffer.
 *
 * Diwas Timilsina 
 * Hw #10 
 */

public class Sieve extends Thread {

    /** The buffer to read from*/
    private final Buffer<Integer> buffer;


    /**
     * Create a sieve with the given input buffer
     */ 
    public Sieve(Buffer<Integer> b){
	buffer = b;
    }

    


    /**
     * Repeatedly read characters from the terminal
     * until eof is seen, putting each character into b.
     */
    public void run(){
	try { 
	    while (true) { 
		int c = buffer.delete(); 
		if (c == -1) break; // -1 is eof  
		System.out.println(c);
		
		Buffer<Integer> out = new Buffer<Integer>(5);
		Sieve another = new Sieve(out);	
		another.start();	

		while(true){
		    int c2 = buffer.delete();
		    if (c2 == -1) {
		     	out.insert(c2);
			return;
		    } else if(c2 % c != 0) {
			out.insert(c2);
		    }
		}

	    } 
	} catch (InterruptedException e) {} 
    } 
    

    // main method for the program 
    public static void main(String args[]) {
	System.out.println("#program starting");
	Buffer<Integer> in = new Buffer<Integer>(5);
	
	Sieve sie = new Sieve(in);
	sie.start();
	
	try {
	    for (int i = 2; i <= 1000; i++){
		in.insert(i);
	    } 
	    in.insert(-1);
	}catch(InterruptedException e){
	    //nothing to do here
	}
    }

}