import scala.actors._
import scala.actors.Actor._

//Type of message received by Sieve 
abstract class Message;

case class Number(n:Int) extends Message;

case object Stop extends Message;

class Sieve(myPrime:Int) extends Actor {

  def act(){ 
    var next:Sieve = null; 
    println(myPrime);
    while(true){
      receive {
 	case Number(n) =>
 	  if (n % myPrime != 0){
	    if(next == null){
	      next = new Sieve(n);
	    }
	    next ! n; 
	  }
	case Stop => 
	  if (next != null){
	    sender ! Stop;
	  }
	exit();
      }
    }
  }
}


//Main program to get things going
object Eratosthenes{
  def main(args: Array[String]) = {
    var first = new Sieve(2);
    for (i <- 3 to 100){
      first ! Number(i); 
    }
  }
  //var list = first !? Stop;
}
