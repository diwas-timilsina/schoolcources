/**
 * Diwas Timilsina
 * Hw # 10
 *
 * */


import scala.actors._
import scala.actors.Actor._

//Type of message received by Sieve 
abstract class Message;

// the message received is a number
case class Number(n:Int) extends Message;

//the message received is a stop message
case object Stop extends Message;


class Sieve(myPrime:Int) extends Actor {
  
  // the next Sieve object that the prime number is 
  // passed onto
  var next:Option[Sieve] = None; 
  
  def act(){ 
    while(true){
      receive {
	// the case when a message received is
	// in the form of a number
 	case Number(n) if (n % myPrime != 0) => {
	  next match {
	    // we don't already have next sieve object
	    // to pass the number onto
	    case None => next = Some(new Sieve(n));
	    // we have the next sieve object to pass the number 
	    // onto
	    case Some(actor) =>  actor ! Number(n);  
	  }	
	}

	// the case when stop message is received
	case Stop => {
	  next match {
	    case Some(actor) => actor !? Stop match {
	      case (rest:List[Int]) => sender ! myPrime::rest; 
	    }
	    case None => sender ! List[Int](myPrime);
	  }
	}
	exit;
      }
    }
  }
  this.start();
}


//Main program to get things going
object Eratosthenes{
  def main(args: Array[String]) = {
    var first = new Sieve(2);
    for (i <- 3 to 100){
      first ! Number(i); 
    }
    println(first !? (Stop));
  }
}
