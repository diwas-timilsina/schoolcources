(*
 * An alternative implementation of Expr using functions
 * Tony Liu and Diwas Timilsina
 *)

type Expr = real * real -> real;

fun buildX() = fn (x,y) => x;
fun buildY() = fn (x,y) => y;
fun buildSine(e:Expr) = fn (x,y) => Math.sin(Math.pi*e(x,y));
fun buildCosine(e:Expr) = fn (x,y) => Math.cos(Math.pi*e(x,y));
fun buildAverage(e1:Expr,e2:Expr) = fn (x,y) => (e1(x,y) + e2(x,y))/2.0;
fun buildTimes(e1:Expr, e2:Expr) = fn (x,y) => e1(x,y) * e2(x,y);
fun buildTanh(e:Expr) = fn (x,y) => ((Math.exp(e(x,y)) - Math.exp(~1.0*e(x,y)))/(Math.exp(e(x,y)) + Math.exp(~1.0*e(x,y))));
fun buildSphere(e1:Expr,e2:Expr,e3:Expr) = fn (x,y) => (Math.pow(e1(x,y),2.0) + Math.pow(e2(x,y),2.0) + Math.pow(e3(x,y),2.0) - 1.0)/2.0;

(*ML cannot convert functions to Strings*)
fun exprToString (e:Expr) = "We have no idea";

(*
 * eval: Expr -> real
 * evaluates Exprs as types, not datatypes
 *)
fun eval (e:Expr) (x,y) = e(x,y);
