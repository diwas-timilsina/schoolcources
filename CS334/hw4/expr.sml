(*
 * expr.sml
 * Tony Liu and Diwas Timilsina
 * cs334
 *)

(* Magic constant to make datatypes print out fully *)
Control.Print.printDepth:= 100;
Control.Print.printLength:= 100;

datatype Expr = 
    VarX
  | VarY
  | Sine     of Expr
  | Cosine   of Expr
  | Average  of Expr * Expr
  | Times    of Expr * Expr;       


(* build functions:
     Use these helper functions to generate elements of the Expr
     datatype rather than using the constructors directly.  This
     provides a little more modularity in the design of your program
*)
fun buildX()             = VarX;
fun buildY()             = VarY;
fun buildSine(e)         = Sine(e);
fun buildCosine(e)       = Cosine(e);
fun buildAverage(e1,e2)  = Average(e1,e2);
fun buildTimes(e1,e2)    = Times(e1,e2);


(* exprToString : Expr -> string
   Complete this function to convert an Expr to a string 
*)
fun exprToString (VarX) = "x"
  | exprToString (VarY) = "y"
  | exprToString (Sine(e)) = "sin(pi*"^exprToString(e)^")"
  | exprToString (Cosine(e)) = "cos(pi*"^exprToString(e)^")"
  | exprToString (Average(a,b)) = "(("^exprToString(a)^"+"^exprToString(b)^")/2)"
  | exprToString (Times(a,b)) = "("^exprToString(a)^"*"^exprToString(b)^")";

(* eval : Expr -> real*real -> real
   Evaluator for expressions in x and y
 *)
fun eval (VarY) (x,y) = y
  | eval (VarX) (x,y) = x
  | eval (Sine(e)) (x,y) = Math.sin(Math.pi*(eval e (x,y)))
  | eval (Cosine(e)) (x,y) = Math.cos(Math.pi*(eval e (x,y)))
  | eval (Average(a,b)) (x,y) = ((eval a (x,y)) + (eval b (x,y))) / 2.0
  | eval (Times(a,b)) (x,y) = (eval a (x,y)) * (eval b (x,y));

val sampleExpr =
      buildCosine(buildSine(buildTimes(buildCosine(buildAverage(buildCosine(
      buildX()),buildTimes(buildCosine (buildCosine (buildAverage
      (buildTimes (buildY(),buildY()),buildCosine (buildX())))),
      buildCosine (buildTimes (buildSine (buildCosine
      (buildY())),buildAverage (buildSine (buildX()), buildTimes
      (buildX(),buildX()))))))),buildY())));

(************** Add Testing Code Here ***************)

(* tests for exprToString *)
exprToString(Times(Sine(VarX),Cosine(Times(VarX,VarY))));
exprToString(Times(Sine(VarX),Average(VarX,VarY)));
exprToString(Times(Cosine((Cosine(VarX))),(Cosine(Cosine(VarX)))));
exprToString(sampleExpr);

(* tests for eval *)
eval (Sine(Average(VarX,VarY))) (0.5,0.0);
eval sampleExpr (0.1, 0.1);


