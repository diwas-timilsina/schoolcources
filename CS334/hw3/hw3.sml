(* 
    HW 3
    Diwas Timilsina and Tony Liu
 *)

Control.Print.printDepth := 100;
Control.Print.printLength := 100;
(* Part A - Basic Functions *)

(* 
 sumSquares: int -> int
 given nonneg int n, returns sum of squares from 1 to n
*)
fun sumSquares 1 = 1
  | sumSquares n = n*n + sumSquares(n - 1);

(* Test functions *)
sumSquares(4);
sumSquares(1);
sumSquares(5);

(* 
 listDup: 'a * int -> 'a list
 Takes element e of any type and nonneg num n, returns a list
 with n copies of e 
*)
fun listDup (e, 1) = e::nil
  | listDup (e, n) = e::listDup(e, n-1);

(* Test functions *)
listDup("moo", 4);
listDup(1, 2);
listDup(listDup("cow", 2), 2);


(* Part B - Zipping and UnZipping *)

(* 
 zip : 'a list * 'b list -> ('a * 'b) list
 computes the product of two lists of arbitrary length
*)
fun zip (nil, nil) = nil
  | zip (x::xs, y::ys) = (x, y)::zip(xs, ys);

zip ([1,3,5,7], ["a", "b", "c", "de"]);

(*
 unzip: 'a * 'b list -> 'a list * 'b list
 Takes a list of tuples of two types and separates them
 into two lists
*)
fun unzip l = 
    let fun unziphelp (nil,a,b) = (a,b)
	  | unziphelp ((x,y)::rest, a,b) = unziphelp(rest,a@(x::nil), b@(y::nil))
    in
	unziphelp(l, [],[])
    end;

unzip([(1,"a"), (2, "b"), (3, "c")]);

(*
 zip3: 'a list * 'b list * 'c list -> 'a * 'b * 'c list
 Computes the product of 3 lists of arbitrary length
*)
fun zip3 (nil,nil,nil) = nil
  | zip3 (x::xs, y::ys, z::zs) = (x,y,z)::zip3(xs,ys,zs);

zip3 ([1,3,5,7],["a","b","c","de"],[2,4,6,8]);

(* Part C - Find *)

(*
 find: ''a * ''a list -> int
 Takes a pair of an (equality) element a and a list of the 
 same type and returns the index (if any) of a. Contains a 
 helper function.
*)
fun find (e, l) = 
  let fun findhelp(a, i, nil) = ~1
	| findhelp(a, i, (x::xs)) = 
	  if(a = x) then i
	  else findhelp(a, i+1, xs)
  in
      findhelp(e, 0, l)
  end;

find(3, [1,2,3,4,5]);
find("cow", ["cow", "dog"]);
find("wombat", ["cow", "dog", "duck"]);

(* Part D - Trees *)

datatype IntTree = LEAF of int | NODE of (IntTree * IntTree);

(*
 sum: IntTree -> int
 returns the sum of the entire IntTree
*)
fun sum (LEAF(x)) = x
  | sum (NODE(x,y)) = sum(x) + sum(y);

sum(LEAF 3);
sum(NODE(LEAF 2, LEAF 3));
sum(NODE(LEAF 2, NODE(LEAF 1, LEAF 1)));

(*
 height: IntTree -> int
 returns the height of the int tree
*)
fun height (LEAF(x)) = 1
  | height (NODE(x,y)) =
    let fun max (x,y) = if(x>y) then x 
			else y
    in
	1 + max(height(x),height(y))
    end;

height(LEAF 3);
height(NODE(LEAF 2, LEAF 3));
height(NODE(LEAF 2, NODE(LEAF 1, LEAF 1)));

(*
 balanced: IntTree -> bool
 Returns whether the IntTree is balanced
*)
fun balanced (LEAF(x)) = true
  | balanced (NODE(x,y)) = 
    (abs(height(x) - height(y)) <= 1) 
    andalso balanced(x) 
    andalso balanced(y);
		    
balanced(LEAF 3);
balanced(NODE(LEAF 2, LEAF 3));
balanced(NODE(LEAF 2, NODE(LEAF 3, NODE(LEAF 2, LEAF 2))));

(* Part E - Stack Operations *)

(* opcodes describing all stack operations *) 
datatype Opcode = 
	 PUSH of real
       | ADD
       | MULT
       | SUB
       | DIV
       | SWAP
       ;

(* defines a real list as a Stack *)
type Stack = real list;

(*
 eval: OpCode list * Stack -> real
 Performs all specified stack operations
*)
fun eval (nil, (a::st):Stack) = a
  | eval (PUSH(n)::ops, st) = eval(ops, (n::st))
  | eval (ADD::ops, a::b::st) = eval(ops, (b+a::st))
  | eval (MULT::ops, a::b::st) = eval(ops, (b*a::st))
  | eval (SUB::ops, a::b::st) = eval(ops, (b-a::st))
  | eval (DIV::ops, a::b::st) = eval(ops, (b/a::st))
  | eval (SWAP::ops, a::b::st) = eval(ops, (b::a::st))
  | eval (_,_) = 0.0;


(* test code *)
eval([PUSH(2.0), PUSH(1.0), ADD],[]);
eval([PUSH(2.0), PUSH(3.0), MULT],[]);
eval([PUSH(2.0), PUSH(1.0), SUB],[]);
eval([PUSH(7.0), PUSH(2.0), DIV],[]);
eval([PUSH(7.0), PUSH(2.0), SWAP,DIV],[]);
eval([PUSH(2.0),ADD],[])
