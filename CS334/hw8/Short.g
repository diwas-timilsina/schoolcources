<start> = 
I have decided to drop my <major> major because <reason>. Instead, I am going to become a Computer Science major.
;

<major> = 
  Math
| Economics
| English
| Chemistry
| History
;


<reason> =
  it is not fulfilling
| it is making me feel stupid
| I am too cool
| I have better uses for my time
| I have been bored out of my mind
;
