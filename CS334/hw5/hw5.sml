(* Diwas Timilsina
* Homework 5*)


(* a).....................................................................*)

fun average (x,y) = (x + y) div 2;

fun ave2 elems = foldr average 0 elems;

fun ave3 elems2 = foldl average 0 elems2;

ave2 [16,8,2];
ave3 [16,8,2];  

(* In this example the input function computes an average of two input values
 * the list is [16,8,2] and the initial value is 0. With this function, list
 * and initial value, the fold right and fold left returns two different values.
 * We can generalize this by saying that any function that is not associative will 
 * produce different results for foldl and foldr.					 
*) 

(*b)..........................................................................*)

fun words_lengths list = foldl (fn (x,y) => size(x) + y) 0 list;

(*example for word_lengths*)
words_lengths ["cow","moo","purple"];
words_lengths nil;


(*c).........................................................................*)
 
fun count v list = foldl (fn (x,y) => if (x=v) then y+1 else y+0) 0 list;

(*example for count*)
count "sheep" ["cow","sheep","sheep","goat"];
count 4 [1,2,3,4,1,2,3,4,1,2,3,4];

(*d).........................................................................*)

fun partition p list = foldr (fn (x,(l1,l2)) => if(x<p) then (x::l1,l2) else (l1,x::l2)) (nil,nil) list; 

(*examples for partation*)
partition 10 [1,4,55,2,44,55,22,1,3,3];


(*e).........................................................................*)

fun poly list x  = foldr (fn (a:real,acc:real) =>  a+x*acc) 0.0 list;

(*examples for ploy*)
val g = poly [1.0,2.0];
g(3.0);

val k = poly[1.0,2.0,3.0];
k(2.0);
