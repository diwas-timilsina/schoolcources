87:2% Milk 
151:2% milk, regular size 
990:All American Slam (w/ Hash browns) 
723:Apple Crisp a la mode 
126:Apple Juice 
470:Apple Pie 
60:Applesauce Musselman's 
85:Applesauce Swimmers_ 
162:Bacon 
610:Bacon, Lettuce & Tomato (French Fries or substitute, & condiments) 
310:Bagel, dry 
220:Baked Potato, plain (w/ Skin) 
110:Banana 
894:Banana Split 
1089:BBQ Chicken Sandwich (French Fries or substitute, & condiments) 
47:BBQ Sauce 
619:Belgian Waffle Platter (Choice of fruit topping or syrup, & margarine) 
192:Biscuit 
775:Blackened Steakhouse Strip 
163:Blue Cheese Dressing 
106:Blueberry Topping 
71:Blueberry Topping 
452:Boca Burger (French Fries or substitute, & condiments) 
508:Boca Burger (w/ Sm fruit bowl) 
100:Bread Stuffing, plain 
190:Broccoli & Cheddar 
344:Brownie 
734:Buffalo Chicken Strips (Condiments) 
974:Buffalo Wings (Condiments) 
296:Burgerlicious_ (Fries or substitute) 
341:Burgerlicious_ (w/ Cheese. Fries or substitute) 
420:Buttermilk Pancake (Choice of fruit topping or syrup, & margarine) 
660:Buttermilk Pancake Platter (Choice of fruit topping or syrup, & margarine) 
133:Caesar Dressing 
100:Cappuccino French Vanilla 
799:Carrot Cake 
100:Cereal (Average of all cereals) 
580:Cheesecake 
365:Chef's Salad (Bread service and dressing choice) 
86:Cherry Topping 
550:Chicken Fried Chicken Dinner (Sides & bread service) 
110:Chicken Noodle 
838:Chicken Ranch Melt (French Fries or substitute, & condiments) 
635:Chicken Strips (Choice of side dishes, & bread) 
720:Chicken Strips (Condiments) 
660:Chicken Tangy Lemon Mushroom (Sides & bread service) 
653:Chocolate Peanut Butter Pie 
133:Chocolate Topping 
90:Cinnamon Apple Filling 
624:Clam Chowder 
694:Classic Burger (French Fries or substitute, & condiments) 
852:Classic Burger (w/ Cheese. French Fries or substitute, & condiments) 
602:Club Sandwich (French Fries or substitute, & condiments) 
701:Coconut Cream Pie 
274:Coleslaw 
110:Corn 
72:Cottage Cheese 
394:Country Fried Potatoes 
644:Country Fried Steak (Choice of side dishes, & bread) 
464:Country Fried Steak & Eggs (Choice of bread, potato or grits) 
1427:Country Sausage Bowl (Choice of bread) 
100:Cream Cheese 
112:Croutons (for Salad) 
164:Cucumber Craverz_ 
240:Deep-Sea Salad_ (w/ Ranch) 
190:Dennysaur_ Chicken Nuggets (Choice of bread, &/or choice of potato, &/or choice of meat. Fries or substitute) 
1090:Denver Scramble (w/ Hash browns. Choice of fruit topping or syrup, & margarine) 
700:Dny Texas Style Steak Tips (Sides & bread service) 
375:Double Scoop/Sundae (Choice of toppings) 
56:Egg Beaters Egg Substitute 
150:English Muffin, dry 
125:English muffin, dry 
1261:Fabulous French Toast Platter (Choice of fruit topping or syrup, & margarine) 
25:Fat Free Ranch Dressing 
958:Fish & Chips (Condiments) 
589:Fish Sandwich 
280:Floats (Root beer or Cola) 
106:French Dressing 
423:French Fries, unsalted 
737:French Silk Pie 
1196:French Slam (Choice of fruit topping or syrup, & margarine) 
627:French-toastix_ (Choice of syrup or margarine) 
438:Fried Chicken Strip Salad (Bread service and dressing choice) 
258:Fried Shrimp Dinner (Choice of side dishes, & bread) 
924:Fruit Filled Pancakes-Apple Cinnamon 
921:Fruit Filled Pancakes-Blueberry 
909:Fruit Filled Pancakes-Cherry 
201:Fudge Topping 
284:Goldfish Bowl 
60:Grape Bowl 
60:Grapefruit 
55:Grapes 
40:Green Beans 
510:Grilled Cheese Sandwich (Fries or substitute) 
190:Grilled Chicken Breast Dinner 
310:Grilled Chicken Breast Salad 
259:Grilled Chicken Breast Salad (Bread service and dressing choice) 
200:Grilled Chicken Dinner (Choice of side dishes, & bread) 
476:Grilled Chicken Sandwich (w/o Dressing. French Fries or substitute, & condiments) 
470:Grilled Tilapia Dinner (Choice of side dishes, & bread) 
80:Grits 
1190:Grnd Slam Slugger_ (w/ Hash brown) 
468:Ham & Cheddar Ome (w/ Eggbeaters. Choice of bread, potato or grits) 
595:Ham & Cheddar Omelette (Choice of bread, potato or grits) 
1110:Ham & Jalapeno Scramble 
1304:Ham & Mushroom Bowl (Choice of bread) 
85:Ham, grilled slice, Honey Smoked 
197:Hashed Browns 
280:Hashed Browns (w/ Cheddar cheese) 
493:Hashed Browns (w/ Onion, cheese, gravy) 
1111:Heartland Scramble (Choice of bread) 
631:Hershey's Chocolate Cake 
766:Hickory Grilled Chicken (Choice of side dishes, & bread) 
160:Honey Mustard Dressing 
100:Hot Chocolate 
344:Hot Fudge Brownie (kids) 
997:Hot Fudge Brownie a la mode 
1134:Italian Chicken Melt (French Fries or substitute, & condiments) 
566:Jr. Dipper (w/ Applesauces & marinara) 
860:Jr. Dippers (w/ Marinara & fries) 
397:Junior Grand Slam (Choice of syrup or margarine) 
100:Kellogg's Dry Cereal (avg) 
773:Lemon Pepper Tilapia (Choice of side dishes, & bread) 
150:Lemonade (w/ Ice) 
15:Low Calorie Italian 
1170:Lumberjack Slam (w/ Hash browns. Choice of fruit topping or syrup, & margarine) 
353:Macaroni & Cheese 
583:Malted Milkshake (van/choc) 
143:Maple-Flavored Syrup 
168:Mashed Potatoes 
168:Mashed Potatoes, plain 
1414:Meat Lover's Bowl (Choice of bread) 
1027:Meat Lover's Breakfast 
1241:Meat Lover's Scramble (Choice of bread) 
560:Milkshake (van/choc) 
2044:Mini Burgers (w/ Onion rings) 
841:Moons Over My Hammy (Choice of potato or grits) 
710:Mozzarella Sticks (Condiments) 
880:Mushroom Swiss Burger 
800:Mushroom Swiss Chopped Steak (Sides & bread service) 
1278:Nacho 
100:Oatmeal 
120:One Egg 
381:Onion Rings 
413:Oodles of Oreo Sundae 
126:Orange Juice 
895:Oreo Blender Blaster 
580:Oreo Blender Blaster (Kids) 
665:Original Grand Slam 
1370:Peach French Toast (Syrup & condiments) 
1430:Pepper Jck & Sm Sausage 
874:Philly Melt (French Fries or substitute, & condiments) 
21:Pico de Gallo 
331:Pizza Party_ 
266:Pot Roast Dinner (Sides & bread service) 
145:Potato Volcano_ (w/ Brown gravy) 
100:Quaker Oatmeal 
129:Ranch Dressing 
78:Raspberry Iced Tea (w/ Ice) 
435:Roast Turkey & Stuffing (Includes gravy. Choice of side dishes, & bread) 
162:Ruby Red Grapefruit Juice 
1405:Sampler_ (Condiments) 
354:Sausage 
295:Sausage Patties 
261:Seasoned Fries 
433:Senior Bacon Cheddar Burger 
700:Senior Belgian Waffle Slam (Choice of bread, &/or choice of potato, &/or choice of meat) 
791:Senior Biscuits & Gravy 
285:Senior Chicken Strip Dinner (Bread; choice of soup, salad or fruit; & vegetable sel) 
540:Senior Club (Fries or substitute) 
341:Senior Country Fried Steak (Bread; choice of soup, salad or fruit; & vegetable sel) 
756:Senior Fish and Chips 
591:Senior French Toast Slam 
149:Senior Fried Shrimp Dinner (Bread; choice of soup, salad or fruit; & vegetable sel) 
200:Senior Grilled Chicken Breast (Bread; choice of soup, salad or fruit; & vegetable sel) 
248:Senior Grilled Tilapia (Bread; choice of soup, salad or fruit; & vegetable sel) 
509:Senior Lemon Pepper Tilapia (Bread; choice of soup, salad or fruit; & vegetable sel) 
429:Senior Omelette (Choice of bread, &/or choice of potato, &/or choice of meat) 
735:Senior Scram. Egg & cheddar (Choice of bread, &/or choice of potato, &/or choice of meat) 
544:Senior Starter_ (Bread; choice of soup, salad or fruit; & vegetable sel) 
360:Senior Turkey & Stuffing (Bread; choice of soup, salad or fruit; & vegetable sel) 
362:Side Caesar (w/ Dressing) 
113:Side Garden Salad (w/o Dressing. Condiments, soup or salad selection) 
113:Side Garden Salad (w/o Dressing) 
188:Single Scoop/Sundae (Delicious dip) (Choice of toppings) 
650:Skinny Moons 
13:Sliced Tomatoes 
490:Slim Slam_ (w/o Topping) 
463:Smiley-face Hotcakes (w/ Meat. Choice of syrup or margarine) 
344:Smiley-face Hotcakes (w/o Meat. Choice of syrup or margarine) 
767:Smothered Cheese Fries (Condiments) 
91:Sour Cream 
880:Spicy Buffalo Chicken Melt (French Fries or substitute, & condiments) 
662:Steakhouse Strip & Eggs (Choice of bread, potato or grits) 
517:Steakhouse Strip & Shrimp 
410:Steakhouse Strip Dinner (Choice of side dishes, & bread) 
115:Strawberry Topping 
77:Strawberry Topping 
23:Sugar-Free Maple-Flavored Syrup 
991:T-bone Steak & Eggs (Choice of bread, potato or grits) 
860:T-bone Steak Dinner (Choice of side dishes, & bread) 
505:Taco Salad (Bread service and dressing choice) 
290:Thanksgiving Jr. 
350:Thanksgiving Jr. (w/ Red grapes) 
334:The Big Cheese (Fries or substitute) 
479:The Super Bird Sandwich (French Fries or substitute, & condiments) 
118:Thousand Island Dressing 
490:Tilapia (w/ Creole. Sides & bread service) 
410:Tilapia/Rice/ Gr. Beans/Tomato Sl. 
92:Toast - Dry 
90:Toast - Dry 
56:Tomato Juice 
248:Turkey Breast Salad (w/o Dressing) 
230:Turkey Breast Salad (w/o Dressing) 
678:Two Eggs and More Breakfast 
619:Ultimate Omelette (Choice of bread, potato or grits) 
79:Vegetable Beef 
79:Vegetable Beef Soup 
173:Vegetable Rice Pilaf 
330:Veggie EB Omelette (w/ English muffin) 
346:Veggie-Cheese Ome (w/ Eggbeaters. Choice of bread, potato or grits) 
494:Veggie-Cheese Omelette (Choice of bread, potato or grits) 
23:Whipped Cream 
23:Whipped Cream, dollop 
87:Whipped Margarine 
1310:Zesty Creole Scrambles 
