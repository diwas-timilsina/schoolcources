/*
 * Scala Pirate Translation
 * CS 334.
 * Tony Liu and Diwas Timilsina
 */

import scala.collection.JavaConversions._
import java.util.StringTokenizer;

class Translator {
  var dict: scala.collection.immutable.Map[java.lang.String,java.lang.String] = Map();

  // Add the given translation from an english word to a pirate word
  def += (k : String, v : String) : Unit = { dict += (k -> v) }
 
  // Look up the given english word.  If it is in the dictionary,
  //    return the pirate equivalent.  Otherwise, just return english.
  def apply(k : String) = { 
    val value = dict.get(k);
    value match {
      case None => k;
      case Some(v) => v;
    }
  }
    
  // Print the dictionary to stdout
  override def toString() = { dict.mkString("\n"); } 
}


object Pirate {
  
  def main(args: Array[String]) {
  
    val punctation = "=+!@#$%^&*():_-|\"\\'<>,.?/[]{}`~ \t\n";

    //part a
    val pirate = new Translator();
    pirate += ("hello", "ahoy");
    val s = pirate("hello");
    println(s);
    println(pirate);

    //Part b
    val lines = scala.io.Source.fromFile("pirate.txt").getLines();
    val data = lines.toList;
    for(i <- data) pirate += (i.substring(0,i.indexOf(":")), i.substring(i.indexOf(":") +1));
    println(pirate);

    //Part c
    // Iterate over each line of stdin
    for(line <- io.Source.stdin.getLines) {
   
      // create iterator for all words on a line of input, when broken
      // apart into tokens.  
      val words = for (s <- new StringTokenizer(line + "\n", 
                                                punctation, 
                                                true)) yield s.toString();
     
      for(x <- words) print(pirate(x)); 
       
    }
  }
}

