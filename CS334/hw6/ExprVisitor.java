
/** 
 * Diwas Timilsina  
 * Hw # 6
 * 
 **/


/** Abstract class for all expressions */
abstract class Expr {
    abstract <T> T accept(Visitor<T> v);
}

class Number extends Expr {
    protected int n;

    public Number(int n) { this.n = n; }

    public <T> T accept(Visitor<T> v) {
	return v.visitNumber(this.n);
    }
}

class Sum extends Expr {
    protected Expr left, right;

    public Sum(Expr left, Expr right) {
	this.left = left;
	this.right = right;
    }

    public <T> T accept(Visitor<T> v) {
	return v.visitSum(left.accept(v), right.accept(v));
    }
}

class Subtract extends Expr {
    protected Expr left, right;

    public Subtract(Expr left, Expr right) {
	this.left = left;
	this.right = right;
    }

    public <T> T accept(Visitor<T> v) {
	return v.visitSubtract(left.accept(v), right.accept(v));
    }
}


class Times extends Expr {
    protected Expr left, right;

    public Times(Expr left, Expr right) {
	this.left = left;
	this.right = right;
    }

    public <T> T accept(Visitor<T> v) {
	return v.visitTimes(left.accept(v), right.accept(v));
    }
}


/** Abstract class for all visitors */
abstract class Visitor<T> {
    abstract T visitNumber(int n);
    abstract T visitSum(T left, T right);
    abstract T visitSubtract( T left, T right);    
    abstract T visitTimes( T left, T right);
}


/** Example visitor to convert an Expr to a String */
class ToString extends Visitor<String> {
    public String visitNumber(int n) { 
	return "" + n;
    }
    public String visitSum(String left, String right) {
	return "(" + left + " + " + right + ")";
    }
    public String visitSubtract(String left, String right) {
	return "(" + left + " - " + right + ")";
    }
    public String visitTimes(String left, String right) {
 	return "(" + left + " * " + right + ")";
    }
}



/** visitor to convert an evaluate an expr tree**/
class Eval extends Visitor<Integer> {
    public Integer visitNumber(int n){
	return n; 
    }  
    public Integer visitSum(Integer left, Integer right){
	return left + right; 
    }
    public Integer visitSubtract(Integer left, Integer right){
	return left - right;
    }
    public Integer visitTimes(Integer left, Integer right){
	return left * right;
    }

}


class Compile extends Visitor<String>{
    public String visitNumber(int n){
	return "PUSH("+ n +")"; 
    }  
    public String visitSum(String left, String right){
	return left + " " + right+ " "+ "ADD"; 
    }
    public String visitSubtract(String left, String right){
	return left + " " + right+ " "+ "SUB";
    }
    public String  visitTimes(String left, String right){
	return left + " " + right+ " "+ "MULT";
    }

 
}

public class ExprVisitor { 
    public static void main(String args[]) { 
	Expr e = new Times(new Number(3), new Subtract(new Number(1),new Number(2)));
	Compile c = new Compile();
	ToString printer = new ToString();
	Compile printer2 = new Compile();
	Eval printer3 =new Eval();
	String s1 = e.accept(printer2);
	Integer s2 = e.accept(printer3);
	String stringRep = e.accept(printer); 
	System.out.println(stringRep);
	System.out.println(s2);
 	System.out.println(s1);
    }

}
