(* Diwas Timilsina
 * Homework 6 *)

(*P1..............................................*)

 
(* a)...............................*)

(*Exception to Handle Non-Numeric Input*)
exception badInput;

(*Convert one character to a numeric digit*)
fun charToNum c = 
    if( #"0" <=c andalso c<= #"9" ) then  ord c - ord#"0" 
    else raise badInput;
	
fun calcList(nil,n)         = n
  | calcList(fst::rest,n)   = calcList(rest,10*n+charToNum fst);
  

(*Convert a string of digits to a number. The explore function converts
 a string to a list of characters*)
fun stringToNum s = 
    (
    calcList(explode s,0)
    )handle badInput => ~1;

(*examples for part a*)
stringToNum "3405";
stringToNum "3a05";

(*b)..................................*)


(*Convert one character to a numeric digit*)
fun charToNum c =  
    if( #"0" <=c andalso c<= #"9" ) then  ord c - ord#"0" 
    else ~1;
	
fun calcList(nil,n)         = n
  | calcList(fst::rest,n)   = 
    if (charToNum (fst) = ~1) then ~1
    else calcList(rest,10*n+charToNum fst);
  

(*Convert a string of digits to a number. The explore function converts
 a string to a list of characters*)
fun stringToNum s = calcList(explode s,0);

(*examples for part b*)
stringToNum "3405";
stringToNum "3a05";


(*c).....................................*)
(* I prefer implementation in part (a) because it is easier to read 
the code in part (a) than in part (b). If someone who didn't write the code
was reading the two different implementations, he/she might not be able to 
say that part b was handling exceptions because it is not obvious.  *)


(*P2)......................................*)

(*a).......................................*)
exception DotProd

fun dotprod l1 l2 = 
    let fun dotprod_tail (nil,nil,t)     = t
	  | dotprod_tail (x::xs,y::ys,t) = dotprod_tail (xs,ys,(x*y)+t) 
	  | dotprod_tail (_,_,_)   = raise DotProd
    in 
	dotprod_tail(l1,l2,0)
    end;

(*examples*)
dotprod [1,2,3] [~1,5,3];
dotprod [~1,3,9] [0,0,11];
dotprod [] [];

(*b).......................................*)

fun fast_fib n = 
    let fun fast_fib_tail (0,t1,t2) = t1
	  | fast_fib_tail (1,t1,t2) = t2
	  | fast_fib_tail (n,t1,t2) = fast_fib_tail(n-1,t2,t1+t2)
    in
	fast_fib_tail (n,0,1)
    end;

(*examples*)
fast_fib 0;
fast_fib 1;
fast_fib 5;
fast_fib 10;









