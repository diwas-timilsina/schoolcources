# Diwas Matt

import sys
from arff import *
import math
import random

class Stump(object):
    def __init__(self, instances, attributes, Dt):#, classes):
        self.Dt = Dt

        self.attributes = attributes

        # Get the INDEX of the max_gain attribute
        self.max_gain_attr = self.max_gain(instances)

        self.majorities = self.majority_classes(instances, self.max_gain_attr)
        
    def entropy(self, S):
        """Get the entropy of instances S"""
        # Get the weighted sum of the number of instances
        n = sum( self.Dt )

        # Get the individual (weighted) frequencies for each of the 2 classes
        counts = {}
        for i in range(len(S)):
            s = S[i]
            # Increment the class counts as seen
            if s[-1] in counts:
                counts[s[-1]] += self.Dt[i]
            else:
                counts[s[-1]] = self.Dt[i]

        # sum of -pi * log2(pi)
        return(sum( [-(counts[c] / n) * math.log(counts[c] / n, 2) for c in counts] ))

    def max_gain(self, S):
        """Return the index in self.attributes of the maximum gain attribute"""
        mg = -1
        A = None
        # Simply compute gain at each attribute, and return the max
        for i in range(0, len(self.attributes[:-1])):
            test = self.gain(S, i)
            if test > mg:
                mg = test
                A = i
        return A
            
    def gain(self, S, i):
        """Compute gain for instances S across self.attributes[i]"""
        Svs = {}
        for key in self.attributes[i]: # Actually just 1 key
            A = self.attributes[i][key]
            for v in A:
                Svs[v] = []

        # Map attribute values to instances that have them
        for s in S:
            Svs[s[i]].append( s )

        # Gain(S, A) = Entropy(S) - SUM |Sv|/|S| * Entropy(Sv)
        return self.entropy(S) - sum( [len(Svs[Sv])/len(S) * self.entropy(Svs[Sv]) for Sv in Svs] ) 

    def majority_classes(self, S, i):
        """Return dict of attrValue => majority classes of split across attribute"""
        majorities = {}

        # Split S across self.attributes[i]
        spl = self.split(S, i)
        for branch in spl:
            # Get majority class among that split branch
            majorities[branch] = self.majority_class(spl[branch])
            
        return majorities

    def majority_class(self, S):
        """Return majority class in set of instances S"""
        counts = {}

        # Loop over instances in S
        for i in range(len(S)):
            s = S[i]
            if s[-1] in counts:
                # Add its weight, rather than 1
                counts[s[-1]] += self.Dt[i]
            else:
                # Add its weight, rather than 1
                counts[s[-1]] = self.Dt[i]

        # Find most common class by frequencies calculated above
        most = -1 
        maj = None
        for c in counts:
            if counts[c] > most:
                most = counts[c]
                maj = c

        return maj

    def split(self, S, i):
        """Split instances in S across self.attributes[i]"""
        spl = {}
        for key in self.attributes[i]:
            A = self.attributes[i][key]
            for v in A:
                spl[v] = []
                for s in S:
                    if s[i] == v:
                        spl[v].append(s)
        return spl

    def h(self, x):
        """Return class for some instance x"""
        # Needs to use the maximum gain attribute we computed earlier
        return self.majorities[x[self.max_gain_attr]]

    
class Boost(object):
    def __init__(self, instances, attributes, classes, T=10, resample_scale=100):
        self.attributes = attributes
        self.T = T
        # Make it so we have the mapping to {-1, +1}
        self.classes = { classes[0] : -1, classes[1] : 1} 
        self.back = { -1 : classes[0], 1 : classes[1] }

        # Initialize distribution
        m = len(instances)
        self.D = [1/m for i in range(m)]

        self.learners = []
        self.alphas = []
        for t in range(T):
            # Train a weak learner using distribution Dt
            wl = Stump(instances, attributes, self.D)
            self.learners.append( wl )

            # Sum all Di where predicted != actual
            eps = sum( [self.D[i] for i in range(len(instances)) if wl.h(instances[i]) != instances[i][-1]] )

            # Get and save the alpha value
            a = self.alpha(eps)
            self.alphas.append(a)
                 
            # get Dt+1(i)
            for i in range(len(self.D)):
                x = instances[i]
                yi = self.classes[x[-1]]
                htxi = self.classes[wl.h(x)]
                # Dt+1(i) = Dt(i) * e^(-alpha * yi * predicted)
                self.D[i] = self.D[i] * math.exp(-a * yi * htxi)

            # Normalization so that D sums to 1 => divide everything by sum of 1
            Zt = sum(self.D)

            # Divide everything by that normalization
            for i in range(len(self.D)):
                self.D[i] /= Zt


    def alpha(self, eps):
        """Get alpha value for this epsilon"""
        return 1/2 * math.log( (1 - eps) / eps )


    def H(self, x):
        """Return the mega hypothesis"""
        # Return the sign of the sum of each alpha_t * hypo_t(x)
        y = (-1, 1)[ sum( [self.alphas[t] * self.classes[self.learners[t].h(x)] for t in range(self.T)] ) > 0]
        
        # Convert back from {-1,+1} to class names
        return self.back[y]


def shuffle(l):
    # Shuffle
    cp = [x for x in l]
    for i in range(len(cp)):
        r = random.randint(0, len(cp) - 1)
        cp[i], cp[r] = cp[r], cp[i]
    return cp


def chunkify(arr, n=10):
    """Divide array into randomized groups of n for n-fold"""
    return [arr[i:i+n] for i in range(0, len(arr), n)]


def flatten(arr):
    """Flatten 2D array into 1D array"""
    return [x for y in arr for x in y]    

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Error: need .arff")
        exit()
    
    f = open(sys.argv[1])
    lines = f.readlines()

    # attributes of the form [{attr : values},...]
    attributes = get_attributes(lines)
    
    # [..., {play : [yes, no]} ]
    classes = [ attributes[-1][key] for key in attributes[-1]][0]
    instances = get_instances(lines)

    # To choose how many folds, we do 10 if the number of instances > 100, else 
    # the number of items in the chunk is just 1    
    chunk_size = (1, 10)[len(instances) > 100]

    if len(instances) > 100:
        instances = shuffle(instances)

    # Divide instances into chunks
    chunks = chunkify(instances, chunk_size)

    correct = 0
    for i in range( len(chunks) ):
        # Hold one chunk out, and combine the rest into the training set
        b = Boost(flatten(chunks[:i] + chunks[i+1:]), attributes, classes, T=10)

        # Get the test set (which may just be a single instance)
        tests = chunks[i]
        for x in tests:
            predicted = b.H(x)
            actual = x[-1]

            if predicted == actual:
                correct += 1

    print("% correct = {p}".format(p = correct / len(instances)))
