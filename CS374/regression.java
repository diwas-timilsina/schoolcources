/**
 * Diwas Timilsina (c) 2014
 * Machine Learning
 *
 * Implementing Linear Regression
 *
 * Procedures to run the program:
 * (1) java regression wi wo size, we feed in wi wo and size of the test data as the 
 *    parametrs for our program
 * (2) The output produces the given equation, the predicted equation, sum of squared errors,
 *    Relative Square Error, Coefficient of Determination
 */


// libraries
import structure.*; 
import java.io.Reader;
import java.util.*;
import java.io.File;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Stack;
import java.util.Random;
import java.util.Iterator;


public class regression {
    
    private Random gen = new Random();
        
    // constructor takes the wi amd wo values of 
    // a linear equation of the form y = wix + wo
    // the constructor also takes size of the training set to be 
    // generated as a prameter
    public regression (double wi, double wo, int size) {
	
	System.out.println("\n############### Equations #####################");
	System.out.println("The given equation is: y = "+wi+"x + "+wo);
	
	// triaining set with noise
	Vector<Pair<Double,Double>>trainingNoise = generateTrainingSet(wi, wo, size, true);
	
	// training set without noise
	Vector<Pair<Double,Double>>training = generateTrainingSet(wi,wo,size, false); 
	    
	// predicted parameters w[0]=w0 amd w[1]=w1
	double [] w = predictParameter(trainingNoise);
	System.out.println("##################################################\n");
	
	System.out.println("################### Error #######################");
	
	computerError(w,training);
	System.out.println("\nNote: If Coefficient of Determination is close to 1, \nthe regression is considered useful");
	System.out.println("##################################################\n");
    }
    
    
    //compute the sum of squared error,relative square error,and coefficient of 
    //determination
    public void computerError(double [] w, Vector<Pair<Double,Double>> training){
	
	double actual_y;
	double predicted_y;
	double error=0.0;
	
	// mean y value without noise to calculate relative square error
	double mean_y =0.0;
		
	for (int i = 0; i< training.size(); i++){
	    actual_y = training.get(i).getRight();
	    predicted_y = w[1]*(training.get(i).getLeft())+w[0];
	    error = error + Math.pow(actual_y-predicted_y,2);
	    mean_y = mean_y + actual_y;
	}
	
	// to compute relative square error
	mean_y = mean_y/ training.size();
	double y_meanSq = 0.0;
	for (int j = 0; j< training.size(); j++){
	    y_meanSq = y_meanSq + Math.pow(training.get(j).getRight() - mean_y, 2);
	}
	
       	System.out.println("The sum of squared errors is: "+ error/2);
	System.out.println("Relative Square Error (RSE): " + error/y_meanSq);
	System.out.println("Coefficient of Determination: " + (1- error/y_meanSq));
    }

    
    //predict the parameter values
    public double [] predictParameter(Vector<Pair<Double,Double>> training){
	double sum_x = 0.0;
	double sum_sq_x = 0.0;
	double sum_r = 0.0;
	double sum_rx = 0.0;

	// treverse the training data and save the required values
	for (int i = 0; i< training.size(); ++i){
	    sum_x = sum_x + training.get(i).getLeft();
	    sum_sq_x = sum_sq_x + Math.pow(training.get(i).getLeft(),2);
	    sum_r = sum_r + training.get(i).getRight();
	    sum_rx = sum_rx + (training.get(i).getLeft())*(training.get(i).getRight());
	} 
	
	// matrix A  
	double [][] A = new double [2][2];
	
	// fill the values for A 
	A[0][0] = training.size() + 0.0;
	A[0][1] = sum_x;
	A[1][0] = sum_x;
	A[1][1] = sum_sq_x;
	
	// matrix y
	double [] y = new double [] {sum_r, sum_rx};
	
	// compute the inverse of A and stick back it in A
	A = computeInverse(A);
	
	//array for the parameters
	double [] w = new double[2];
	
	//compute the parameters
	w [0] = A[0][0] * y[0] + A[0][1]*y[1];
	w [1] = A[1][0] * y[0] + A[1][1]*y[1];
	
	//print out the predicted equation
	System.out.println("The predicted equation is: y = "+w[1]+"x + "+w[0]);
	
	return w;
	    
    }
    
    
    // calculate the inverse of the given matrix
    public double [][] computeInverse ( double [][] matrix){
	
      	Assert.pre((matrix.length * matrix[0].length) == 4, "Incorrect Martrix size, should have been 2*2");
	
	// compute the determinant of the matrix first
	double determinant = (matrix[0][0] * matrix[1][1]) - (matrix[0][1] * matrix[1][0]);
	
	if ( determinant > 0){
	    double a = matrix[0][0]/determinant;
	    double b = -1.0*matrix[0][1]/determinant;
	    double c = -1.0*matrix[1][0]/determinant;
	    double d = matrix[1][1]/determinant;
	    
	    matrix[0][0]= d;
	    matrix[1][1]= a;
	    matrix[0][1]= b;
	    matrix[1][0]= c;
	} else {
	    Assert.fail("Given matrix cannot have an inverse");
	}
	
	return matrix;
    }
    

    // Generate a pair from a normal distribution with
    // the specified mean and variance.
    public Pair <Double,Double>  noise(double mean, double variance) {
	double u1, u2;
	double v1, v2;
	double x = 0.0;
	double y, xPrime, yPrime;
	double s;
	do {
	    u1 = gen.nextDouble();
	    u2 = gen.nextDouble();
	    v1 = 2 * u1 - 1;
	    v2 = 2 * u2 - 1;
	    s = v1*v1 + v2*v2;
	} while (s >= 1 || s == 0);

	x = Math.sqrt(-2 * Math.log(s) / s) * v1;
	y = Math.sqrt(-2 * Math.log(s) / s) * v2;

	xPrime = mean + Math.sqrt(variance) * x;
	yPrime = mean + Math.sqrt(variance) * y;
	
	return new Pair <Double,Double> (xPrime, yPrime);
    }

    
    // generate the training set based on the equation parameters and the size of the
    // data set to be generated and whether we would like to add noise to the data
    public Vector<Pair<Double,Double>> generateTrainingSet(double wi, double wo, int size, boolean addNoise){
	
	// vector to store the intput attribute value x and corresponding output value r
	// (x, r) is the format of the pair 
	Vector<Pair<Double,Double>> data = new Vector<Pair<Double,Double>>();
	
	
	// for pair returened by noise (x,noise)
	Pair<Double,Double> rnd_pair;
	double noise;
	double x;
	double r;
	double noise_value = 0.0;
	
	// generate the data according to the specified size
	for(int i = 0; i < size; i++){
	    rnd_pair = this.noise(0,2.25);
	    x = rnd_pair.getLeft();
	
	    if (addNoise == true){
		noise_value = rnd_pair.getRight(); 
	    }
	   
	    r = wi * x + wo + noise_value;
	    data.add( new Pair<Double,Double>(x,r));
	}
	
	return data;
    }


    // main function of the class
    // that takes wi wo and size of the data set to be generated as
    // the parameters.
    public static void main(String argv[]){
    
	// get the parameters of the linear equation from 
	// the standard input 
	double wi = Double.parseDouble(argv[0]);
	double wo = Double.parseDouble(argv[1]);
	int size = Integer.parseInt(argv[2]);
	
	// initialize the class 
	regression linear = new regression(wi,wo,size);
    }
}



 