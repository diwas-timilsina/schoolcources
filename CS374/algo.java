 /**
 * Diwas Timilsina (c) 2014 
 * Machine Learning  
 *
 *  Naive Bayes Learner and classifier with laplace estimator
 */


// libraries
import structure.*; 
import java.io.Reader;
import java.util.*;
import java.io.File;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Stack;
import java.util.Random;
import java.util.Iterator;



public class algo{
    
    // filename
    protected String filename; 
 
    // vector that stores hash table for string to integer mapping
    protected Vector<Hashtable<String,Hashtable<String,Double>>> table; 
 
    // Vector to store class values 
    protected Vector<String> classValues;
    
    // vector to store the attributes 
    protected Vector<String> attr_list;
    
    // count of how many data entires are there
    protected int data_count = 0;

    //total number of correct counts
    protected int correct_count = 0;

    public algo(String file) {
	try {

	    // vector to store the table
	    table = new Vector<Hashtable<String,Hashtable<String,Double>>>();
	    
	     // vector to store the class values
	    classValues = new Vector<String>();
	    
	    //declare the scanner 
	    Scanner scan = new Scanner(new File(file));
	    
	    // single line read form the file
	    String line;
	    
	    //vector for attributes 
	    attr_list = new Vector<String>();
	    
	    // data lines
	    String data = ""; 
	    
	    
	    // line with the class values
	    String class_line="";
	    
	    
	    // List to shuffle the data
            java.util.List<String> shuffleData = new ArrayList<String>();
	    

	    // while there are lines to read, parese the lines 
	    while (scan.hasNext()){
		
		line = scan.nextLine().toLowerCase();
		
		if (line.startsWith("@relation")){
		    saveName(line);
		}else if (line.startsWith("@attribute")){
		    
		    int start;
		    int end;
		    
		    while(line.startsWith("@attribute")){
			start = line.indexOf("{");
			end = line.indexOf("}",start);
			// line without the the brackets
			line = line.substring(start+1,end);
			
			// replace the commas in the line with a blank space
			line = line.replace(',',' ');
			
			attr_list.add(line);
			line = scan.nextLine().toLowerCase();
		    }
		    
		    // grab one attribute at a time and create a table with the values
		    StringTokenizer st = new StringTokenizer(attr_list.get(attr_list.size()-1));
		    while(st.hasMoreElements()){
			classValues.add(st.nextToken());
		    }
		    
		    // parse the attributes
		    saveAttribute();

		    // parese the data
		}else if (line.startsWith("@data")){
		    line = scan.nextLine().toLowerCase().replace(',',' ');
		    data_count++;  
		    while(line.startsWith("%")){
			line = scan.nextLine().toLowerCase().replace(',',' ');
		    }
		    data = data + line + "\n"; 
		    while(scan.hasNext()){
			line = scan.nextLine().toLowerCase().replace(',',' ');
			if (!line.startsWith("%")){
			    shuffleData.add(line);
			    data = data+line +"\n";
			    data_count++;
			}
		    }
		}		
	    }
	    
	    
	    
	    
	    // if data count is less than 100 then use the
	    // same data for training and test
	    // else 75% of the data goes to training and remaining
	    // 25% goes to testing
	    if (data_count < 100){
		training(data);
		testing(data);
	   
		System.out.println("\nNumber of Examples Classified Correctly: "+correct_count);
		System.out.println("Number of Examples Classified Incorrectly: "+ (data_count - correct_count));
		
		
	    }else {
		
		// randomize the data first
		Collections.shuffle(shuffleData);
		
		String training_data="";
		String testing_data="";
		double training_limit=(0.75*data_count);
		int index = 0;
		int training_count = 0;
		
		
		// sepearte training set and testing data set
		Iterator<String> iter = shuffleData.iterator();
		while (iter.hasNext()){
		    if (index < training_limit){
			
			training_data= training_data+iter.next()+"\n";
			
			
		    } else {
			testing_data=testing_data+iter.next()+"\n";
			training_count++;
		    } 
		    index++;
		}
		
				
		training(training_data);
		testing(testing_data);
		
		System.out.println("\nNumber of Examples Classified Correctly: "+correct_count);
		System.out.println("Number of Examples Classified Incorrectly: "+ (training_count - correct_count));
	    
	    } 
	    
	    
	    //close the scanner	       
	    scan.close();
	}catch(Exception e){
	    System.out.println(e);
	}
	
    }

    //training data
    void training (String data){
	
	String assignClass=""; 
  	int start = 0;
	int next = data.indexOf("\n");
	String one_line ="";
	
	
	while (start < data.length()){
	    one_line = data.substring(start,next); 
	    for (int i =0; i< classValues.size(); i++){

		// get the class value of the data
		if(one_line.contains(classValues.get(i))){
		    assignClass = classValues.get(i);
		    break;
		}
			
	    }
	    
	    start = next+1;
	    next = data.indexOf("\n",start);
	    

	    int value=0 ;
	    String token = "";
	    int table_index = 0;
	    StringTokenizer st = new StringTokenizer(one_line);
	    while (st.hasMoreElements() && table_index < table.size()){
		
		//deal with the last entry of the table seperately
		if (table_index == table.size()-1){
		    st.nextToken();
		    token = "class";
		    
		}else{
		    token = st.nextToken();
		}
		
		
		// if the entry is missing then ignore it else parse it
		if (token.equals("?")){
		    table_index++;
		} else {
		    
		    
		    Hashtable<String,Hashtable<String,Double>> attr_table = table.get(table_index);
		    
		    // new hashtable for the class values
		    Hashtable<String,Double> newValue = new Hashtable<String,Double>();
		    

		    // update the class values frequencies for different attributes 
		    for (int i = 0; i< classValues.size(); i++){
			if(classValues.get(i).equals(assignClass)){
		
			    newValue.put(assignClass, attr_table.get(token).get(assignClass) +1.0 );			
			}else {

			    newValue.put(classValues.get(i),attr_table.get(token).get(classValues.get(i)));
			}
		    }
		    

		    // added the updated hashtable to the attribute 
		    table.get(table_index).put(token,newValue);
		    
		    table_index++;
		    
		}
	    }
	    
	}
    }
    
    
    // testing the data and producing output
    void testing (String data){

	// start and end of the each line 
	int start = 0;
	int next = data.indexOf("\n", start);

	// one line at a time
	String one_line ="";

	// one token (attribute value) at a time
	String token = "";

	//to store class
	String class_value = "";

	// class total 
	double class_total = 0.0;
	
	//total likelihood across all classes
	double total_prob;

	//likelihood
	double likely;


	// table to store the likelihood of all the classes
	Hashtable<String,Double> likelihood;

	while (start < data.length()){
	    likelihood = new Hashtable<String,Double>();
	    
	    System.out.println("\n#####################################");
	 
	    one_line = data.substring(start,next); 
	    
	    System.out.println("Data: " +one_line.replace(' ', ','));
	    
	    // total probablities
	    total_prob = 0.0;
	    
	   	   
	    for (int i = 0; i < classValues.size(); i++){
		likely = 1.0; 
	
		StringTokenizer st = new StringTokenizer(one_line);
		class_value = classValues.get(i);
 		
		class_total = table.get(table.size()-1).get("class").get(class_value);

		likely = likely * class_total/data_count; 
		
	
		// index of frequency table
		int table_index = 0 ; 
		while(st.hasMoreElements() && (table_index < table.size()-1)){
		
		    token = st.nextToken();
		
		    if (token.equals("?")){
			table_index++;
		    }else{
			likely = likely * ((table.get(table_index).get(token).get(class_value))/class_total);
			table_index++;		    
			
		    }
		    
		    if (likely == 0.0) {
			table_index = 0;
			break;
		    }
		}
	
		// Laplace estimator 
		// if one of the likelyhood is zero read the line again 
		// add one to the numerator values and add value equal to the number of 
		// attributes to the denominator
		if (likely == 0.0){

		    System.out.println(one_line);
		    StringTokenizer st2 = new StringTokenizer(one_line);
		    likely = class_total/data_count; 
		    class_total = class_total + attr_list.size()-1;
		  

		    while(st2.hasMoreElements() && (table_index < table.size()-1)){
						
			token = st2.nextToken();
			
			if (token.equals("?")){
			    table_index++;
			} else {
			    
			    likely = likely * ((table.get(table_index).get(token).get(class_value)+1)/class_total);
			    table_index++;		    
			}
		    }
		}
				
		
		// total probablities
		total_prob= total_prob+likely;
	
		// add the class likelihood to the map
		likelihood.put(class_value,likely);
	    }
	    
	    
	    

	    // now assign a class
	    String assigned_class= "";
	    double max_prob= 0.0;
	    String temp = "";
	    Enumeration <String> en = likelihood.keys(); 
	    while (en.hasMoreElements()){
		temp = en.nextElement();
		System.out.println("Probablity of "+ temp +" ="+((likelihood.get(temp))/total_prob));
		 
		if (((likelihood.get(temp))/total_prob) > max_prob){
		    max_prob = ((likelihood.get(temp))/total_prob);
		    assigned_class = temp;
		} 
	    }

	    
	    //Assign class
	    System.out.println("\nAssigned Class: " + assigned_class);

	    //target class
	    StringTokenizer st3 = new StringTokenizer(one_line);
	    String target = "";
	    while (st3.hasMoreElements()){
		target = st3.nextToken();
	    }
	    
      	    System.out.println( "Target Class: " + target); 	    
	    
	    // count the correct number of assignments
	    if(assigned_class.equals(target)){
		correct_count++;
	    }
	    
	    	    
	    start = next+1;
	    next = data.indexOf("\n",start);
	    System.out.println("#####################################");
	  	    
	}
	
    }
    
    
    
    
     
    //name of the file
    void saveName(String line){
	StringTokenizer st = new StringTokenizer(line);
	while (st.hasMoreElements()){
	    st.nextElement();
	   
	}
    }


    // save the attributes and create a table
    void saveAttribute(){
	String line;
	Hashtable <String,Double> attr_map = new Hashtable<String,Double>();
	for (int j = 0; j< classValues.size(); j++){
	    attr_map.put(classValues.get(j),0.0);
	}
	
	for (int i = 0; i< attr_list.size(); i++){
	   
	    line = attr_list.get(i);
	    Hashtable<String,Hashtable<String,Double>> map = new Hashtable<String,Hashtable<String,Double>>();
	   
	    
	    if ( i == attr_list.size()-1){
		map.put("class",attr_map);
	    } else {
		 // grab one attribute at a time and create a table with the values
		StringTokenizer st = new StringTokenizer(line);
		while(st.hasMoreElements()){
		    map.put(st.nextToken(),attr_map);
		}
	    } 
	    table.add(map);
	}
    }
   
    
    // main 
    public static void main(String argv[]) {
	algo machine = new algo(argv[0]);
    }
    
}