# Diwas Timilsina
# K-means Clustering

import sys
from arff import *
import math
import random

def has_converged(old,new):
    #check to see if old mean is the same as new mean
    for i in range(len(old)):
        for j in range(len(new)):
            if (old[i][j] != new[i][j]):
                return False
    return True

def find_center(x,means):
    #find the closest center to a point
    min_dis = float("inf")
    best = None 
    for i in range(len(means)):
        m = means[i]
        dis = math.sqrt(sum((float(x[i])-float(m[i]))**2 for i in range(len(m)))) 
        if ( dis < min_dis):
            min_dis = dis
            best = m
    return best

def compute_mean(listCol):
    #compute the mean eg..[...[(x+y+...)/n, ..., ]...]
    mean = []
    for i in range(len(listCol[0])-1):
        mean.append(sum(float(l[i]) for l in listCol)/ len(listCol))
    return mean         

def cluster_points(instances,means):
    #find the clusters of the instances
    clusters={}
    for inst in instances:
        best_center = find_center(inst[:-1],means)
        try:
            clusters[repr(best_center)].append(inst)
        except KeyError:
            clusters[repr(best_center)] = [inst]
    return clusters

def recalculate_center(clusters):
    # recalculate the centers of the clusters
    new_means = []
    for k in clusters.keys():
        new_means.append(compute_mean(clusters[k]))
    return new_means


def majority_class(inst):
    # return the majority class in set of instances
    counts = {}

    # Loop over instances in inst
    for i in range(len(inst)):
        s = inst[i]
        if s[-1] in counts:
            counts[s[-1]] += 1
        else:
            counts[s[-1]] = 1
            
    # Find most common class by frequencies calculated above
    most = -1 
    maj = None
    for c in counts:
        if counts[c] > most:
            most = counts[c]
            maj = c

    return maj

def find_majority(clusters,classes):
    #return a dict of cluster label => majority class across all the examples 
    #in that cluster 
    majorities = {}
    
    for k in clusters.keys():
        values = clusters[k]
        maj = majority_class(values)
        majorities[k] = maj
    return majorities
  
def compute_error(clusters,classes):
    #computer the error rate based on majority vote
    majority_list = find_majority(clusters,classes)
    count = 0
    total = 0 
    for k in clusters.keys():
        values = clusters[k]
        for v in values:
            total+=1
            if not v[-1] == majority_list[k]:
                count += 1
    print(count/total *100)
    print(total)
    
if __name__=="__main__":
    if len(sys.argv) < 2:
        print("Error: need .arff")
        exit()

    f = open(sys.argv[1])
    lines = f.readlines()
    
    # attributes from the dataset
    attributes = get_attributes(lines)
    
    # [..., {play : [yes, no]} ]
    classes = [ attributes[-1][key] for key in attributes[-1]][0]
    k = len(classes)
    instances = get_instances(lines)

    #initial guesses for the means
    new_means = get_random(instances,k)           
   
    #previous means 
    previous_means=[]   
    for means in new_means:
        ind_means = []
        for x in means:
            ind_means.append(0)
        previous_means.append(ind_means)
   
    # compute clusters and check for error rate    
    while not (has_converged(previous_means,new_means)):
        cluster = cluster_points(instances,new_means)
        previous_means = new_means
        new_means = recalculate_center(cluster)
        
    compute_error(cluster,classes)
