import re
import random

def split(s):
    """Regex split for handling different possible delimiters"""
    return re.split("[ |,] *", s)

def get_attributes(lines):
    """Return an array of attributes.
    attributes[i] is a list of the form ["young",...]"""
    attributes = []
    for line in lines:
        l = line.strip().lower()
        if l.startswith("@attribute"):
            #attributes.append(split(l[l.find("{")+1 : l.find("}")]))
            key = l.split(" ")[1]
            values = split(l[l.find("{")+1 : l.find("}")])
            attributes.append( {key : values} )
        elif l.startswith("@data"):
            break
    #for attr in attributes[:-1]:
    #    attr.append("?")
    return attributes

def get_instances(lines):
    """Return all instances from the .arff file"""
    instances = []
    begin = False
    for line in lines:
        l = line.strip().lower()
        if begin and not l.startswith("%"):
            instances.append( [token.lower() for token in split(l)] )
        elif l.startswith("@data"):
            begin = True
    return instances

def divide_instances(instances, train_size):
    """Divide the instances into training and test set."""
    # Shuffle copy of the list, so we can take just the first x elements
    inscopy = [ inst for inst in instances ]
    random.shuffle( inscopy )
    train_set = inscopy[:train_size]
    test_set  = inscopy[train_size:]
    return train_set, test_set
