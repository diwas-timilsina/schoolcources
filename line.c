
#include<stdio.h>
#include<stdlib.h>

// A simple program that prints the first nth line 
// of the standard input
// (c) 2013 Diwas Timilsina

main(int argc, char **argv)
{
  if(argc<2){
    printf("Invalid Input! Provide file name and an integer\n");
    exit(1);
 }else{
    int n = atoi(argv[1]);
    char input[BUFSIZ];
    int line = 0;
    
    //read each line at a time, and increment 
    // the counter line with it. When the counter
    // line reaches the line that we want to print
    // stop reading and print the line
    while(fgets(input, BUFSIZ , stdin)){
      line++;
      if(line == n){
	fputs(input,stdout);
	break;
      }
    }
  }
}
