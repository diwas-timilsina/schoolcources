;;; Phase 3 WARM interpreter
;;; (c) 2013 Diwas Timilsina

;;; --------------------------------------
;;;  WIND registers and their uses
;;;  	 r0 --->
;;;	 r1 --->    current instruction that is being executed 	
;;; 	 r2 --->    copy of the source register 
;;;      r3 --->
;;;      r4 --->
;;;      r5 --->    warm Ccr register	
;;;      r6 --->
;;;      r7 --->
;;;      r8 --->
;;;      r9 --->     pointer to the WARM program written here
;;;      r10 --->    
;;;      r11 ---->   condition code bits (29-31)
;;;      r12 --->    opcode bits (23-28)	
;;;      r13 --->    destination register bits (19-22) 	
;;;      r14 --->    source register bits (15-18)
;;; 	 r15 --->    remaining/ right hand source (0-14)
;;;      rip
;;;      ccr
;;; ---------------------------------
	
	;; ----------------------
	;; renaming the registers
	;; ----------------------
	.requ	srccpy, r2
	.requ	cond, r11
	.requ 	opcode, r12
	.requ	dest, r13
	.requ	src, r14
	.requ	rem, r15
	.requ	wccr, r5
	
;;; load the WARM program to be interpreted
	lea 	warm, r0
	trap	$SysOverlay
	mov	r0, r9
	mov	$0xffffff, wsp

;;; decoding starts here
loop:
	mov	wpc, r3
	mov	0(r9, r3), r1	; fetch the next instruction and store it in r1

	;; condition code bits (29-31)
	mov	r1, cond
	shr	$29, cond
	mov	condTable(cond),cond
	jmp	(cond)

;;; execute only when Z = 1
eq:	mov	eqTable(wccr),cond
	jmp	(cond)
	
;;; execute only when Z = 0 
ne:	mov	neTable(wccr),cond
	jmp	(cond)

;;; execute only when N != V
lt:	mov	ltTable(wccr),cond
	jmp 	(cond)

;;; exectue when Z = 1 or N != V
le:	mov	leTable(wccr),cond
	jmp	(cond)

;;; execute when N = V
ge:	mov	geTable(wccr),cond
	jmp	(cond)

;;; execute when Z = 0 and N = V
gt:	mov	gtTable(wccr),cond
	jmp	(cond)

;;; Never execute the instruction
no:
	add	$1, wpc
	and	$0xffffff, wpc
	jmp 	loop

;;; exectue the instruction
yes:
 	;; source register
	mov	r1, src
	shr	$15, src
	and	$0xf, src
	mov	src, srccpy
	mov	registers(src),src
	

	;; destination register
	mov	r1, dest
	shr	$19, dest
	and	$0xf, dest

	;; opcode
	mov	r1, opcode
	shr	$23, opcode
	and	$0x3f, opcode

;;; Main Decoding starts
	test 	$0x10, opcode	; test bit 27
	jg	branch1
	test	$0x4000, r1	; test bit 14
	jg	other
;; immediate arithematic mode
	and	$0x7fff, r1
	mov	r1, rem
	and	$0x1ff, rem
	shr	$9,r1
	shl	r1,rem
	mov	instructions(opcode),opcode
	jmp	(opcode)			;opcode reached

branch1:
	test	$0x8,opcode	; test bit 26
	je	other
;; type 2 branch instruction
	mov	r1, rem
	and	$0xffffff,rem
	mov	instructions(opcode),opcode
	jmp	(opcode)			; opcode reached

other:
	and	$0x7fff, r1
	mov	r1, rem
	shr	$10, r1
	mov	decode(r1),r1
	jmp 	(r1)
	
;;; register inderect with Immediate displacement mode
branch2:
	mov	rem,r8
	or	$0xffffc000, r8
	test	$0x2000,rem
	cmovne	r8,rem
	mov	instructions(opcode),opcode
	jmp	(opcode)			; opcode reached
	
	
;;; register logical left  shifted by immediate mode
R1lsl:	and	$0x3ff, rem
	mov	rem, r3
	shr	$6, rem
	and	$0x3f, r3
	mov	registers(rem),rem
	shl	r3, rem
	mov	instructions(opcode),opcode
	jmp	(opcode)			; opcode reached

;;; register  shifted by immediate mode/ immediate indexing mode
R1lsr:	and	$0x3ff, rem
	mov	rem, r3
	shr	$6, rem
	and	$0x3f, r3
	mov	registers(rem),rem
	shr	r3, rem
	mov	instructions(opcode),opcode
	jmp	(opcode)			; opcode reached	

R1asr:	and	$0x3ff,rem
	mov	rem, r3
	shr	$6, rem
	and	$0x3f, r3
	mov	registers(rem),rem
	sar	r3, rem
	mov	instructions(opcode),opcode
	jmp	(opcode)			; opcode reached

R1ror:	and	$0x3ff, rem
	mov	rem, r3
	shr	$6, rem
	and	$0x3f, r3
	mov	registers(rem),rem
	mov	rem, r7
	shr 	r3, rem
	mov	$32, r0
	sub	r3,r0
	shl	r0, r7
	add 	r7, rem
	mov	instructions(opcode),opcode
	jmp	(opcode)			; opcode reached

;;; register shifted by register mode 
R2lsl:	and	$0x3ff,rem
	mov	rem, r3
	shr	$6, rem
	and	$0xf,r3
	mov	registers(rem), rem
	mov	registers(r3), r3
	shl	r3, rem
	mov	instructions(opcode),opcode
	jmp	(opcode)			; opcode reached
	
R2lsr:	and	$0x3ff, rem
	mov	rem, r3
	shr	$6, rem
	and	$0xf,r3
	mov	registers(rem), rem
	mov	registers(r3), r3
	shr	r3, rem
	mov	instructions(opcode),opcode
	jmp	(opcode)			; opcode reached
	
R2asr:	and	$0x3ff, rem
	mov	rem, r3
	shr	$6, rem
	and	$0xf,r3
	mov	registers(rem), rem
	mov	registers(r3), r3
	sar	r3, rem
	mov	instructions(opcode),opcode
	jmp	(opcode)			; opcode reached
	
R2ror:
	and	$0x3ff,rem
	mov	rem, r3
	shr	$6, rem
	and	$0xf,r3
	mov	registers(rem),rem
	mov	registers(r3),r3
	mov	rem,r7
	shr	r3,rem
	mov	$32,r0
	sub	r3,r0
	shl	r0,r7
	add	r7,rem
	mov	instructions(opcode),opcode
	jmp	(opcode)			; opcode reached

;;; Register Product Mode
R3:	and	$0x3ff, rem
	mov	rem, r3
	shr	$6,rem
	and	$0xf, r3
	mov	registers(rem),rem
	mov	registers(r3),r3
	mul	r3,rem
	mov	instructions(opcode),opcode
	jmp	(opcode)			; opcode reached
	

;;; add instruction
doAdd:
	add	$1, wpc
	and	$0xffffff, wpc
	add	rem, src
	mov	src, registers(dest)
	jmp	loop

;;; add instruction that sets condition code bits
doAdds:
	add	$1, wpc
	and	$0xffffff, wpc
	add	rem, src
	mov	src, registers(dest)
	mov	ccr, wccr
	jmp	loop

;;; add with carry instruction
doAddc:
	add	$1, wpc
	and	$0xffffff, wpc
	cmp	$2, wccr
	mov	$0, r0
	cmovg	$1, r0	
	add	r0,rem 
	add	rem, src
	jmp	loop

;;; add with carry instruction that sets condition code bits
doAddcs:
	add	$1, wpc
	and	$0xffffff, wpc
	cmp	$2, wccr
	mov	$0, r0
	cmovg	$1, r0	
	add	r0,rem 
	add	rem, src
	mov	ccr, wccr
	jmp	loop

;;; cmp instruction(same for both type)
doCmp:
	add	$1,wpc
	and	$0xffffff,wpc
	cmp	rem,src
	mov	ccr, wccr
	jmp	loop

;;; and instructions
doAnd:
	add	$1, wpc
	and	$0xffffff,wpc
	and 	rem,src
	mov	src,registers(dest)
	jmp	loop


;;; and instructions that sets condition code bits
doAnds:
	add	$1, wpc
	and	$0xffffff,wpc
	and 	rem,src
	mov	src,registers(dest)
	mov	ccr, wccr
	jmp	loop

;;; multiply
doMul:
	add	$1, wpc
	and	$0xffffff,wpc
	mul	rem,src
	mov	src,registers(dest)
	jmp	loop

;;; mul with condition bits set
doMuls:
	add	$1, wpc
	and	$0xffffff,wpc
	mul	rem,src
	mov	src,registers(dest)
	mov	ccr, wccr
	jmp	loop

doDiv:
	add	$1,wpc
	and	$0xffffff, wpc
	div	rem, src
	mov	src, registers(dest)
	jmp	loop

doDivs:
	add	$1,wpc
	and	$0xffffff, wpc
	div	rem, src
	mov	src, registers(dest)
	mov	ccr, wccr
	jmp	loop

doSub:
	add	$1, wpc
	and	$0xffffff, wpc
	sub	rem,src
	mov	src, registers(dest)
	jmp 	loop

doSubs:
	add	$1, wpc
	and	$0xffffff, wpc
	sub	rem,src
	mov	src, registers(dest)
	mov	ccr,wccr
	jmp 	loop

doEor:
	add	$1, wpc
	and	$0xffffff,wpc
	xor 	rem,src
	mov	src, registers(dest)
	jmp	loop
doEors:
	add	$1, wpc
	and	$0xffffff,wpc
	xor 	rem,src
	mov	src, registers(dest)
	mov	ccr, wccr
	jmp	loop

doOrr:	
	add	$1,wpc
	and	$0xffffff,wpc
	or	rem,src
	mov	src,registers(dest)
	jmp	loop

doOrrs:	
	add	$1,wpc
	and	$0xffffff,wpc
	or	rem,src
	mov	src,registers(dest)
	mov	ccr,wccr
	jmp	loop

doTst:
	add	$1, wpc
	and	$0xffffff,wpc
	test	rem,src
	mov	ccr, wccr
	jmp	loop

doMla:
	add	rem,src
	mov	src, registers(dest)
	add	$1,wpc
	and	$0xffffff,wpc
	jmp	loop
	
doMlas:
	add	$1,wpc
	and	$0xffffff,wpc
	add	rem,src
	mov	src, registers(dest)
	mov	ccr, wccr
	jmp	loop
	
doMov:
	add	$1,wpc
	and	$0xffffff,wpc
	mov	rem, registers(dest)
	jmp	loop

doMovs:
	add	$1,wpc
	and	$0xffffff,wpc
	mov	rem, registers(dest)
	test	$0xffffffff,rem
	mov	ccr, wccr
	and	$12, wccr
	jmp	loop

doMvn:
	add	$1,wpc
	and	$0xffffff, wpc
	xor	$0xffffffff,rem
	mov	rem, registers(dest)
	jmp	loop

doMvns:
	add	$1,wpc
	and	$0xffffff, wpc
	xor	$0xffffffff,rem
	mov	rem, registers(dest)
 	test	$0xffffffff,rem
	mov	ccr, wccr
	and	$12, wccr
	jmp	loop
		
doLdr:
	add	rem,src
	and	$0xffffff,src
	mov	src, r8
	add	$1, wpc
	and	$0xffffff, wpc
	mov	0(r9, r8), r8
	mov	r8, registers(dest)
	jmp	loop

doLdrs:
	add	rem,src
	and	$0xffffff,src
	mov	src, r8
	add	$1, wpc
	and	$0xffffff, wpc
	mov	0(r9, r8), r8
	mov	r8, registers(dest)
	test	$0xffffffff,r8
	mov	ccr, wccr
	and	$12, wccr
	jmp	loop

	
doStr:
  	add	rem,src
	and	$0xffffff,src
	mov	registers(dest),dest
	mov	dest, r8
	add	$1,wpc
	and	$0xffffff,wpc
	mov	r8,0(r9,src)
	jmp	loop

doStrs:
	add	rem,src
	and	$0xffffff,src
	mov	registers(dest),dest
	mov	dest, r8
	add	$1,wpc
	and	$0xffffff,wpc
	mov	r8,0(r9,src)
	test	$0xffffffff,r8
	mov	ccr, wccr
	and	$12, wccr
	jmp	loop

doAdr:
	add	$1, wpc
	and	$0xffffff,wpc
	add	rem, src
	and	$0xffffff, src
	mov	src, registers(dest)
	jmp	loop
	
doB:
	add	rem,wpc
	and	$0xffffff,wpc
	jmp	loop

doBl:
	mov	wpc, wlr
	add	$1, wlr
	and	$0xffffff,wlr
	add	rem, wpc
	and	$0xffffff,wpc
	jmp	loop
	
doSwi:
	add	$1,wpc
	and	$0xffffff,wpc
	and	$0xf,rem
	mov	wr0,r0
	trap 	rem
	mov	r0, wr0
	jmp	loop

doSwis:	
	add	$1,wpc
	and	$0xffffff,wpc
	and	$0xf,rem
	mov	wr0,r0
	trap	rem
	mov	r0, wr0
	test	$0xffffffff,wr0
	mov	ccr,wccr
 	jmp	loop

doLdm:
	add	$1, wpc
	and	$0xffffff, wpc
	mov	$0, r8
	mov	dest, r10
	mov	registers(dest),dest
next1a:
	mov	rem, r0
	cmp	$15, r8
	jg	done1a
	shr	r8,r0
	test	$1, r0
	cmovne	0(r9,dest),registers(r8)
	add	$1, r8
	test	$1, r0
	je	next1a
	add	$1,dest
	and	$0xffffff,dest
	jmp	next1a
done1a:
	mov	dest,registers(r10)
	jmp	loop
	
doLdms:
	add	$1, wpc
	and	$0xffffff, wpc
	mov	$0, r8
	mov	dest, r10
	mov	registers(dest),dest
next1b:
	mov	rem, r0
	cmp	$15, r8
	jg	done1b
	shr	r8,r0
	test	$1, r0
	cmovne	0(r9,dest),registers(r8)
	add	$1, r8
	test	$1, r0
	je	next1b
	add	$1,dest
	and	$0xffffff,dest
	jmp	next1b
done1b:
	mov	dest,registers(r10)
	cmp	$0, r0
	je	loop
	test	$0xffffffff,registers(r8)
	mov	ccr, wccr
	jmp	loop


doStm:
	mov	$0, r8
	mov	dest, r10
	mov	registers(dest),dest
	mov	$0, r6

counta:	mov	rem, r0
	cmp	$15, r8
	jg	donecounta
	shr	r8, r0
	add	$1, r8
	and	$1, r0
	je	counta
	add	$1, r6
	jmp	counta

donecounta:
	mov	$0, r8
	sub	r6, dest
	and	$0xffffff,dest
	mov	dest, r7
next2a:
	mov	rem,r0
	cmp	$15,r8
	jg	done2a
	shr	r8,r0
	test	$1,r0
	cmovg	registers(r8),0(r9,dest)
	mov	dest,r3
	add	$1,dest
	and	$0xffffff, dest
	test	$1,r0
	cmove	r3,dest
	add	$1,r8
	jmp	next2a
done2a:
	add	$1, wpc
	and	$0xffffff, wpc
	mov	r7,registers(r10)
	jmp	loop
	
	
doStms:
	mov	$0, r8
	mov	dest, r10
	mov	registers(dest),dest
	mov	$0, r6

countb:	mov	rem, r0
	cmp	$15, r8
	jg	donecountb
	shr	r8, r0
	add	$1, r8
	and	$1, r0
	je	countb
	add	$1, r6
	jmp	countb

donecountb:
	mov	$0, r8
	sub	r6, dest
	and	$0xffffff,dest
	mov	dest, r7
next2b:
	mov	rem,r0
	cmp	$15,r8
	jg	done2b
	shr	r8,r0
	test	$1,r0
	cmovg	registers(r8),0(r9,dest)
	mov	dest,r3
	add	$1,dest
	and	$0xffffff, dest
	test	$1,r0
	cmove	r3,dest
	add	$1,r8
	jmp	next2b  
done2b:
	add	$1, wpc
	and	$0xffffff, wpc
	mov	r7, registers(r10)
	cmp	$0, r0
	je	loop
	test	$0xffffffff,registers(r8)
	mov	ccr,wccr
	jmp	loop
	
doLdu:
	add	$1, wpc
	and	$0xffffff,wpc
 	and	$0xffffff,src
	mov	src, r10
	add 	rem, src
	and	$0xffffff,src
	mov	src, r4
	test	$0x80000000, rem
	cmove	r10, src
 	mov	0(r9,src),registers(dest)
	mov	r4, registers(srccpy)
	jmp	loop
doLdus:
	add	$1, wpc
	and	$0xffffff,wpc
	and	$0xffffff,src
	mov	src, r10
	add 	rem, src
	and	$0xffffff,src
	mov	src, r4
	test	$0x80000000, rem
	cmove	r10, src
	mov	0(r9,src),registers(dest)
	mov	r4, registers(srccpy)
	test	$0xffffffff,registers(dest)
	mov	ccr, wccr
	and	$12, wccr
	jmp	loop
doStus:
	add	$1, wpc
	and	$0xffffff,wpc
	and	$0xffffff,src
	mov	src,r10
	add	rem,src
	and	$0xffffff,src
	mov	src, r4
	mov	registers(dest),dest 
	test	$0x80000000,rem
	cmove 	r10,src
	mov	dest,0(r9,src)
	mov	r4, registers(srccpy)
	test	$0xffffffff,dest
	mov	ccr, wccr
	and	$12, wccr
	jmp	loop
	
doStu:
	add	$1, wpc
	and	$0xffffff,wpc
	and	$0xffffff,src
	mov	src,r10
	add	rem,src
	and	$0xffffff,src
	mov	src, r4
	mov	registers(dest),dest
	test	$0x80000000,rem
	cmove	r10,src
	mov	dest,0(r9,src)
	mov	r4, registers(srccpy)
	jmp	loop
	

;;; Invalid instruction
Invalid:
	trap	$SysHalt
	
;;; ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ All the tables ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;; ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
;;; ------------------
;;; condition Table
;;; ------------------
condTable:
		.data 	yes, no, eq, ne, lt, le, ge, gt

;;;---------------------------------------
;;; yes refers to exectue the instruction
;;; no refers to just increment the program counter and
;;; jump back to loop
;;; --------------------------------------

;;; equal table for all the combinations of N,Z,C,V bits
eqTable:
		.data	no, no, no, no, yes, yes, yes, yes, no, no, no, no, yes, yes, yes, yes

;;; not equals table 
neTable:
		.data	yes, yes, yes, yes, no, no, no, no, yes, yes, yes, yes, no, no, no, no

;;; less than table 
ltTable:
		.data	no, yes, no, yes, no, yes, no, yes, yes, no, yes, no, yes, no, yes, no

;;; less than equal
leTable:
		.data	no, yes, no, yes, yes, yes, yes, yes, yes, no, yes, no, yes, yes, yes, yes

;;; greater than and equal
geTable:
		.data	yes, no, yes, no, yes, no, yes, no, no, yes, no, yes, no, yes, no, yes

;;; greater than table
gtTable:
		.data	yes, no, yes, no, no, no, no, no, no, yes, no, yes, no, no, no, no


;;; ---------------------------------
;;; table to decode right hand source
;;; ---------------------------------
decode:
	.data	branch2, branch2, branch2, branch2, branch2,  branch2,  branch2,  branch2,  branch2,  branch2,  branch2,  branch2,  branch2,  branch2,  branch2,  branch2, R1lsl, R1lsr, R1asr, R1ror, R2lsl, R2lsr, R2asr, R2ror, R3, R3, R3, R3, Invalid, Invalid, Invalid, Invalid 
	
;;; --------------------------------
;;; Table for executing instructions
;;; --------------------------------
instructions:
		.data	doAdd, doAddc, doSub, doCmp, doEor, doOrr, doAnd, doTst, doMul, doMla, doDiv, doMov, doMvn, doSwi, doLdm, doStm, doLdr, doStr, doLdu, doStu, doAdr, Invalid, Invalid, Invalid, doB, doB, doBl, doBl, Invalid, Invalid, Invalid, Invalid, doAdds, doAddcs, doSubs, doCmp, doEors, doOrrs, doAnds, doTst, doMuls, doMlas, doDivs, doMovs, doMvns, doSwis, doLdms, doStms, doLdrs, doStrs, doLdus, doStus, doAdr, Invalid, Invalid, Invalid, doB, doB, doBl, doBl, Invalid, Invalid, Invalid, Invalid   

;;; ----------------------------
;;; Scratch Pad 
;;; -----------------------------
registers:
wr0:
	.data	0
wr1:
	.data	0
wr2:
	.data	0
wr3:
	.data	0
wr4:
	.data	0
wr5:
	.data	0
wr6:
	.data	0
wr7:
	.data	0
wr8:
	.data	0
wr9:
	.data	0
wr10:
	.data	0
wfp:
	.data	0
wr12:
	.data	0
wsp:
	.data 	0
wlr:
	.data	0
wpc:
	.data	0

;;; -----------------
;;; warm program stored here
;;; -----------------
warm:
	.data	