# include <stdio.h>
# include <stdlib.h> 

// The program reads in finite number of integers and 
// writes them out in non decreasing order
// (c) 2013 Diwas Timilsina

// compare finction used for sorting
int compare(void *apt, void *bpt){
  int *ap = (int*)apt;
  int *bp = (int*)bpt;
  return (*ap)-(*bp);
}


// main function of the program
int main (int argc, char ** argv){
    // allocated size 
  int nalloc = 6;
  
  // pointer to the allocated memory
  int *ip = (int*)malloc(nalloc*sizeof(int));
  
  // current number of elements in the array in the memory
   // pointed by ip
  int currentSize = 0;
  
  //current value
  int value;

  // scan for integers as long as you find them
  while(1 == scanf("%d", &value)){

    // reallocate the memory if we run out of space
    if(currentSize == nalloc){
      nalloc =2*nalloc;
      // double the size of nalloc
      ip= (int*)realloc(ip,nalloc*sizeof(int));
    }
   ip[currentSize++]= value;
  }
  
  // sort the elements
  qsort(ip,currentSize,sizeof(int),compare);

  // print the elements in the sorted array pointed by ip
  int i = 0;
  for (i; i< currentSize; i++){
    printf("%d\n", ip[i]);
  }

  // free the memory
  free(ip);

  return 0; 
}
