#include<stdio.h>
#include<stdlib.h>

// A simple program to check whether a 
// system is little endian or big endian
//
// (c) 2013 <Diwas Timilsina>

int main (int argc, char **argv)
{
  // a hexadecimal number 
  int testNum = 0x04030201;

  // pointer to testNum
  int * ip = &testNum;

  // cating the int pointer into an int 
  // pointer
  char *cp = (char *)ip;
  
  //if the pointer is pointing to 04,
  // then the most significant bit appear in the 
  // upper left corner of the memory. Thus, the 
  // system is big endian
  if(*cp == '\004'){
    printf("Turst me, Your Machine is big endian\n"); 
  } 

  //if the pointer is pointing to 01, 
  //then the least significant bit appears in the upper
  // left corner of the memory. Thus , the system is 
  // little endian

  if(*cp =='\001'){
    printf("Trust me, Your Machine is little endian\n"); 
  }

  return  0;  
}
