#include <stdio.h>
#include <stdlib.h>
// the program reads a file from input and echoes it back as output
// all the tabs in input file are converted into correct number of 
// spaces in the output file. 
// (c) 2013 Diwas Timilsina


int main (int argc,char ** argv)
{
  
  // counter to keep track of number of 
  // characters read 
  int count = 0;

  // current character from the standard in 
  char input;

  // while the end of file hasn't reached
  // if the grabbed character is tab then
  // compute the corresponding number of spaces
  // and print them. Otherwise just print the 
  // character
  while((input = getchar())!= EOF){
    
    // takes care of the new line 
    if (input == "\n"){
      putchar('\n');
      count = 0; 
    }else if (input == '\t'){
     // prinst the correct number of
     // space
      do{
	putchar(' ');
	count++;
      } while(count%8);
    }else{
      putchar(input);
    }
    count++;
  }
  return 0;
}

