# A method to perform insertion sort, based on "insertionSort" of sortie.c
# (c) 2013 <Diwas Timilsina>
	.text
	.globl	isort
	.type	isort,@function

	# The stack frame
	# Duane 		   -48
	# size (3rd variable)	   -44
	# n (2nd variable) 	   -40	
	# nm1 (local variable)     -36
	# c (4th variable)         -32
	# data (1st parameter) 	   -24		
	# right (local variable)   -16
	# left (local variable)	   -8	
	# old frame pointer (rbp)  +0
	# return address	   +8

	.equ	left, -8
	.equ    right, -16
	.equ    data, -24
	.equ    c, -32
	.equ    nm1, -36
	.equ    n, -40
	.equ    size, -44
isort:
	# base pointer saves the location of stack pointer
	# and allocate enough space in the stack
	#for the variables
	pushq	 %rbp  		
	movq 	%rsp, %rbp  
	subq	$48,%rsp

	# save the parameters in the stack
	movq 	%rdi, data(%rbp)
	movl	%esi, n(%rbp)
	movl  	%edx, size(%rbp)
	movq    %rcx, c(%rbp)

	# nm1 = n-1
	movl	n(%rbp),%eax 		# move n to eax
	subl	$1, %eax	
	movl	%eax, nm1(%rbp)

	# if statement
	cmpl	$1, n(%rbp)		# 1 ?? n	
	jle	overif			# if (n<=1), jump to overif

	# insertion sort call
	movq 	data(%rbp),%rdi		#move data into rdi
	movl	nm1(%rbp),%esi		#move nm1 into esi
	movl	size(%rbp),%edx		#move size into edx
	movq	c(%rbp),%rcx		#move c into rcx
	call	isort			# call isort (data, nm1, size, c)

	# compute right
	movq	data(%rbp),%rax	 	# right = data
	movq	%rax,right(%rbp)	
	movl	$0,%edx			# clear out edx 
	movl	nm1(%rbp),%eax		# move nm1 into eax
	movl	size(%rbp),%ecx		# move size into ecx
	mulq	%rcx			# compute size*nm1
	addq	%rax,right(%rbp)	# add the result to right
	
	# compute left
	movq	right(%rbp),%rax	# left = right
	movq	%rax, left(%rbp)	 
	movl	size(%rbp),%eax		# left = left - size
	subq	%rax,left(%rbp)
	

	# loop starts
loop:
	movq	data(%rbp),%rax		# data ?? left 
	cmp	%rax, left(%rbp)	
	jl	endloop			# if left < data, break out of the loop
	movq	left(%rbp),%rdi		
	movq	right(%rbp),%rsi
	movq	c(%rbp), %r9		
	call 	*%r9			# *c(left,right)
	cmpl	$0,%eax			# result ?? 0
	jle	endloop			# if result < = 0, jump to endloop

	#memswap
	movl	size(%rbp),%edi			
	movq	left(%rbp),%rsi
	movq	right(%rbp),%rdx
	call 	memswap			# memswap(size,left,right)

	#change left and right
	movl	size(%rbp),%eax
	subq	%rax,left(%rbp)
	subq	%rax,right(%rbp) 

	jmp	loop
	
endloop:	
overif:	
	
	# move the base pointer back to the
	# stack pointer
	# return the result
	movq	%rbp, %rsp
	popq	%rbp	
	ret
	.size	isort,.-isort

