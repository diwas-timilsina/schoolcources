#include <stdio.h>
#include <stdlib.h>

// This program takes a five digit access code
// and produces all the possible number of combinations
// of codes that is possible. However, there are two numbers
// in each button: (1,2) (3,4) (5,6) (7,8) (9,0). so each 
// digit has two possiblity. 
// 
// (c) 2013 Diwas Timilsina


int main ( int argc, char ** argv)
{
  //to store the five digit integers
  int num[5];
  int count = 0;  
  for(count; count< 5; count++){
    num[count] = argv[1][count]-'0';
  }

  // the index of the array stores the other
  // value that is on the same button as the 
  // indexed number. 
  int table[10] = {9,2,1,4,3,6,5,8,7,0};
  
  
  int i = 0; // keeps track of subsets
  int j = 0; // keeps track of bit shifting

  // the outer for-loop is for the 32 possible subsets 
  // of the access code. 
  // the inner for-loop is to keep track of the bit shifting and
  // the elements in the array num
  for (i = 0; i< 32; i++){
    for (j = 0; j < 5 ; j++){
      if (i & (16>>j)){
	printf("%d",table[num[j]]);
      } else {
	printf("%d", num[j]);
      }
    } 
     putchar('\n');
  }
  return 0;
}

