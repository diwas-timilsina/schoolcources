
// A simple program to find the closest prime numbers 
// from a given number
// 
// (c) 2013 Diwas Timilsina

#include <stdio.h>


// function isPrime takes a integer as a parameter
// and checks if the integer is a prime or not 
int isPrime(int n)
{
  
  if ( n== 2) return 1;
  if (n%2 == 0) return 0;
  if (n < 2) return 0;
  int  f = 3;
  while (f*f <= n){
    if (n%f == 0) return 0; 
    f+=2;
  }
  return 1;
}


//main function for the program
int main(int argc, char *argv[])
{
  // acts as a boolean to check whether or not 
  // the nearest prime is found
  int foundPrime = 0;   

  //distance from the given number
  int countSteps = 1;

  int n = (argc == 1) ? 13 : atoi(argv[1]);

  //checks until a nearest  prime number(s) from the given number is 
  //found.

  if (isPrime(n)){
    printf("The nearest Prime is %d\n",n);
  } else{
    while(!foundPrime){
      if ((n-countSteps) > 1 && isPrime(n-countSteps)){
	printf("The nearest Prime is %d\n", n-countSteps);
	foundPrime=1;
      }
      if (isPrime(n+countSteps)){
	printf("The nearest Prime is %d\n", n+countSteps);
	foundPrime=1;
      }
      // increse the distance from the given number
      countSteps++; 
    }
  }
  return 0;
}
