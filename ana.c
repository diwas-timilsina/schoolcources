# include <stdio.h>
# include <stdlib.h>
// The program reads the unix dictionary 
// (/usr/share/dict/words) and prints out those
// words that are anagrams of the given string
//
//(c) 2013 <Diwas Timilsina>


// compare function for the sorting
// elements in a array alphabetically
int compare(void *ap, void *bp){
  return (*(char*)ap)-(*(char*)bp);
}

char* alphaStr( char* dest, char* src){
  int i; 
  int j = 0;
  for (i=0; i < strlen(src); i++){
    if(isalpha(src[i])){
      dest[j] =toupper(src[i]);
	j++;
    }
  }
  dest[j] = '\0';
  return dest;
}  


// main function of the program
int main (int argc, char ** argv)
{

  
  // string to save the input
  char str[strlen(argv[1])];
  strdup(argv[1]);

  // copy the result of alphaStr to str
  strcpy(str, alphaStr(str,argv[1]));
     
  // sort the string according to alphabetic order
  qsort(str,strlen(str),sizeof(char),compare);

  //file pointer
  FILE * pfile;

  // read the unix dictionary
  pfile = fopen ("/usr/share/dict/words", "r");

  // to temporarily store the words from the dictionary
  char temp[BUFSIZ];
  
  // sorted temp array
  char tempSort[strlen(str)];

  
  // while the end of the file hasn't reached,
  // scan for the strings and store it in 
  // temp. 
  while(fscanf(pfile,"%s",&temp[0]) == 1){
    
    char word[strlen(temp)];
    strcpy(word,alphaStr(word,temp));
    
    // just check for the words with same lengths
    if (strlen(word) == strlen(str)){
      
      strcpy(tempSort,word);
      // sort the tempsort array
      qsort(tempSort, strlen(tempSort),sizeof(char),compare);

      // checks if the tempSort has the same elements as str
      // if that is the case than prints the actual words
      // saved in temp before sorting
      if (!strcmp(str,tempSort)){
	printf("%s\n",temp);
      }
    }
  }
  // close the stream
  fclose(pfile);
  return 0;
}

   



