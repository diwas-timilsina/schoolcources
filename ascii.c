# include <stdio.h>
# include <stdlib.h>
// A simple program that converts ascii value into 
// numeric value and also converts numbric value into 
// ascii values
// (c) 2013 Diwas Timilsina

// convert binary string to integer
int bs2i(char* str){
  int n = 0;
  int i; 
  for(i = 0; i < strlen(str); i++){
    n = 2*n + (str[i] - '0');
  }
  return n; 
}


// convert integer to binary
void i2bs(int n){
  if (n<=1){
    printf("%d",n);
  }else{
    i2bs(n/2);
    printf("%d",n%2);
  }
}

// function responsible for printing 
// numbers
void printall(int input){
  //character
  printf("%c\n",input);
   
  // binary
  printf("%s","0b");
  i2bs(input);
  putchar('\n');
  
  // octal
  printf("%s","0");
  printf("%o\n",input);

  //decimal 
  printf("%d\n",input);

  //hexadecimal 
  printf("%s","0x");
  printf("%x\n",input);
  
}

//the main function
int main(int argc, char **argv){

  // if the program is ascii
  // print the numberical values of 
  // the asci character
  if (!strcmp(argv[0],"ascii")){
    int input  = argv[1][0];
    printall(input);  
     
    
     
     // if it is a char the print the 
     // ascii representation as well as
     // the numeric represenation of the
     // value
  }else if(!strcmp(argv[0],"char")){  
    char * str = argv[1];
    char *tempstr;
    int  temp;
    int in;

    // if the character is a binary number
    if (str[1]=='b'){
      strcpy(tempstr,&str[2]);
      printall(bs2i(tempstr));

      // if the character is a decimal number,
      //octal number or a hexadecimal number
    }else if(sscanf(str,"%i",&temp)){
      printall(temp);
      
      
    } 
  }   
}

