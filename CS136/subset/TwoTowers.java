import structure5.*;
import java.util.Iterator;

/*
 * Diwas Timilsina
 *
 * This class runs finds the best subset of blocks.  It takes n blocks and
 * uses the subset iterator to find the possible subsets of them.  It takes
 * the sum of each subset and remembers the one that comes closest to the 
 * total height/2.  That is printed as the best subset
 *
 */

public class TwoTowers{
    
    public static void main (String [] args){
	// The number of blocks
	int n = Integer.parseInt(args[0]);

	// A vector of doubles that contains the square roots of 1 through n
	Vector<Double> vect = new Vector<Double>();
	for (int i = 1; i < n+1; i++){
	    vect.add(Math.sqrt(i));
	}

	// The iterator
	SubsetIterator<Double> iter = new SubsetIterator<Double>(vect);
	
	// Variables to represent the sums
	double bestSum;
	double currentSum;

	// variables representing the best subset and the current
	Vector <Double> bestSubSet = iter.get();
	Vector <Double> currentSubSet = bestSubSet;

	// The best sum starts as the sum of the first best
	bestSum = sum(bestSubSet);
	// The height is the sum of the square roots of 1 through n
	double height = sum(vect);

	// Uses iterator to go through subsets
	while(iter.hasNext()){
	    currentSubSet = iter.next();
	    // Finds the sum of the current subset
	    currentSum = sum(currentSubSet);

	    // If the sum of the current subset is between the best sum
	    // and the height/2 it becomes the new best
	    if(bestSum < currentSum && currentSum < (height/2)){
		bestSum = currentSum;
		bestSubSet = currentSubSet;
	    }
	}
	// Prints the best subset and its sum
	System.out.println(bestSubSet);
	System.out.println("Sum" + bestSum);
    }

    // Finds the sum of a vector
    private static Double sum(Vector<Double> v){
	// sum starts at 0 and it goes through the vector adding each element
	double sum = 0;
	for(int i = 0; i < v.size(); i++){
	    sum = sum + v.get(i);
	}
	return sum;
    }

} 
