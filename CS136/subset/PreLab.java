

// Diwas Timilsina 

public class CharacterIterator extends AbstractIterator<Character>{
    
    private String input;
    int count; 

    public CharacterIterator(String str){
	input = str;
	reset();
    }
    
    public Character next(){
	return input.charAt(count++);
    }

    public boolean hasNext(){
	return count < = input.length();
    }

    public void reset(){
	count = 0;
    }
    
    public Character get(){
	return input.charAt(count);
    }
}