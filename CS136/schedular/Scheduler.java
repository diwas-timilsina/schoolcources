import java.util.Iterator;
import structure5.*;
import java.util.Scanner;

/*
 * Diwas Timilsina
 * CS 136
 */

/*
 * This class takes a file containing students and the four classes that they 
 * are taking and makes a final exam schedule so that no student has two
 * exams at the same time.  It prints both the schedule by exam slots and 
 * the schedule by class alphabetically.
 */

public class Scheduler{
    
    // Variables for the graph, scanner, and graph iterator
    protected Graph<String, Integer> graph;
    protected Scanner scan;
    protected Iterator<String> iter;

    // Variables for the vector of slots and the vector of courses and their
    // slot numbers
    protected Vector<Vector<String>> data;
    protected Vector<ComparableAssociation<String,Integer>> vec;
    
    
    // Constructor for the scheduler, taking a scanner as a parameter
    public Scheduler(Scanner aScanner){
	// Creates a graph, the vector of slots, and the vector of courses
	graph = new GraphListUndirected<String,Integer>();
	data = new Vector <Vector<String>>();
	vec = new Vector<ComparableAssociation<String,Integer>>();

	// Sets the scanner as the variable scanner
	scan = aScanner;
	
	// Adds all students' courses  to the graph and the vectors
	add();
	addSlots();
	addOrdered();
    }

    // Adds all of the courses to the graph, with edges between courses shared
    // by a student
    public void add(){
	// Goes through scanner in groups of five
	while(scan.hasNextLine()){
	    // The first line scanned is not important for our purposes
	    scan.nextLine();
	    String s[] = new String[4];

	    // For the next 4 lines, add course to graph and to an array
	    for(int i = 0; i < 4; i++){
		String vert = scan.nextLine();
		s[i] = vert;
		graph.add(vert);
	    }
	    
	    // Add edges between all of the four vertices using the array of
	    // courses
	    for(int n = 0; n < 3; n++){
		String v1 = s[n];
		for(int j = n+1; j < 4; j++){
		    String v2 = s[j];
		    graph.addEdge(v1,v2,1);
		    
		}
	    }
	}	
    }

    // Add courses to the slots and then adds the slots to the vector of slots
    public void addSlots(){
	while (graph.size()>0){
	    // gets a random vertex
	    iter = graph.iterator();
	    if(iter.hasNext()){
		// Makes a vector to represent the slot
		Vector<String> vect = new Vector<String>();
		// Adds the vertex to the slot
		String vert = iter.next();
		vect.add(vert);
		// Goes through the rest of the vertices
		while(iter.hasNext()){
		    String v2 = iter.next();
		    boolean different = true;
		    
		    // If a vertex shares an edge with one of the courses in
		    // the slot, then do nothing
		    for(int i = 0; i < vect.size(); i++){
			String ve = vect.get(i);
			if(graph.containsEdge(ve,v2)){
			    different = false;
			    break;
			}
		    }
		    // Otherwise add it to the slot
		    if(different){
			vect.add(v2);
		    }
		}	
		// Add the slot to the vector of slots
		data.add(vect);
		
		// Remove the courses in that slot from the graph
		remove(vect);
	    }
	}
    }
    
    // Goes through the courses in the slot and removes them from the graph
    public void remove(Vector<String> vect){
	for(int i = 0; i < vect.size(); i++){
	    graph.remove(vect.get(i));
	}
	
    }
    
    // Prints the schedule by slot
    public void printVector(){
	// Goes through the vector of slots and for each one, prints the courses
	for(int i = 0; i < data.size(); i++){
	    int n = i + 1;
	    String s = "Slot " + n + ": " +  print(data.get(i));
	    System.out.println(s);
	}
    }

    // Prints the courses in a slot
    public String  print(Vector<String> str){
	String result = " ";
	// Goes through the slot vector and prints each course 
	for(int i =0; i < str.size(); i++){
	    result = result + str.get(i)+ " ";
 	}
	return result;
    }
    
    // Adds the courses to a vector of alphebetical courses
    protected void addOrdered(){
	// Goes through each slot
	for(int i = 0; i< data.size(); i++){
	    int n = i+1;
	    
	    // Goes through every course in a slot and adds it to the vector
	    // based on where it goes alphebetically, with the slot number
	    // as its value as a comparable association
	    for(int j=0; j< data.get(i).size(); j++){
		ComparableAssociation<String,Integer> assoc = 
		    new ComparableAssociation<String,Integer>(data.get(i).get(j), n);
		int location = locate(assoc);
		vec.add(location,assoc);
	    }
	}
    }
    
    // Prints the courses althabetically with their slot number
    public void printOrdered(){
	// Goes through the alphabetical vector and prints the course and its 
	// slot
	for(int i = 0; i< vec.size(); i++){
	    String result = " " + vec.get(i).getKey() + " " + vec.get(i).getValue();
	    System.out.println(result);
	}
    }
    
    // Computes the index in the vector that a course should go alphabetically
    // using a binary search
    protected int locate(ComparableAssociation assoc){
	int low = 0;
	int high = vec.size();
	int mid = (low+high)/2;
	
	while(low<high){
	    ComparableAssociation midAssoc = vec.get(mid);
	    if (midAssoc.compareTo(assoc) < 0){
		low = mid + 1;
	    } else{
		high = mid;
	    }
	    
	    mid = (low+high)/2;
	}
	return low;
    } 
    
    // Prints out all the vertices of the graph by iterating over all of them
    public void printGraph(){
	while(iter.hasNext()){
	    System.out.println(iter.next());
	}
	
    }
    
    
    
    // Takes the input and constructs the scheduler.  Prints the exam schedule
    // both by slot and alphabetically by course.
    public static void main (String args[]){
	Scanner scan = new Scanner(System.in);
	Scheduler sched = new Scheduler(scan);
	sched.printVector();
	System.out.println("--------------------------------");
	sched.printOrdered();
	
    }
}