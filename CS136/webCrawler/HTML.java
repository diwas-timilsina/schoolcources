


/**
 *  Diwas Timilsina
 *  9th April 2013
 *  CS 136 
 *
 * This program builds a simple version of web crawler and determines the distance from one page to another. 
 * If page A references to Page B, the distance between Page A and Page B is 1. However, Page B may not have link 
 * to page A, so distance from Page B to A may not be defined. Also, the distance from Page A to itself is distance 0. 
 */



import java.io.*;
import java.net.*;
import structure5.*;
import java.util.Iterator;
import java.util.Scanner;

public class HTML extends AbstractIterator implements Comparable
{
    protected URL url;
    protected String content;
    protected List linkList;
    protected String link;
    protected int pagesize;
    protected AbstractIterator theIterator;

    public HTML(String l)
    {
        this(l,10*1024);
    }

    public HTML(String l, int ps)
    // pre: l is a valid URL
    // post: returns a structure that references a web object
    {
        pagesize = ps;
        link = l;
        try
        {
            url = new URL(link);
            link = url.toExternalForm();
        } catch (Exception e)
        {
            url = null;
        }
        content = null;
        linkList = null;
    }

    public String content()
        // post: returns a string that consists of the first page of the URL
    {
        return content(pagesize);
    }

    public String content(int pageSize)
        // pre: pageSize >= 0
        // post: reads up to pageSize characters from the remote URL;
        //       returned as a string
    {
        if (content == null)
        {
            content = "";
            try
            {
                URLConnection conn = url.openConnection();
                if (link.startsWith("http:") &&
                    (conn.getContentType().indexOf("text/html") != -1))
                {
                    InputStream in = (InputStream)url.getContent();
                    byte text[] = new byte[1024];
                    do
                    {
                        if (pageSize <= 0) break;
                        int len = in.read(text,0,Math.min(1024,pageSize));
                        if (len == -1) break;
                        pageSize -= len;
                        content = content + new String(text,0,len);
                    } while (true);
                    in.close();
                }
            } catch (Exception e){};
        }
        return content;
    }

    public int compareTo(Object o)
        // post: comparison is based on full URL
    {
        HTML other = (HTML)o;
        return this.link.compareTo(other.link);
    }

    public boolean equals(Object o)
        // post: equals is based on full URL
    {
        HTML other = (HTML)o;
        return this.link.equals(other.link);
    }

    public String toString()
        // post: returns the URL
    {
        return link;
    }

    public boolean hasNext()
    {
        if (theIterator == null)
        {
            theIterator = (AbstractIterator)links();
        }
        return theIterator.hasNext();
    }
    
    public String nextURL()
    {
        return (String)theIterator.next();
    }

    public Object next()
    {
        return theIterator.next();
    }

    public Object get()
    {
        return theIterator.get();
    }

    public void reset()
    {
        theIterator.reset();
    }


    public Iterator links()
        // post: returns a list of http: links found on first page.
        //       cgi-bin scripts not included.
        {       
            return links(pagesize);
        }

    public Iterator links(int pageSize)
        // pre: pageSize >= 0
        // post: returns the http: links found on the page of size
        //      pageSize
    {   
        if (linkList == null)
        {
            linkList = new SinglyLinkedList();
            if (link.startsWith("http:") &&
                -1 == link.indexOf("/cgi-bin/"))
            {
                try {
                    URLConnection conn = url.openConnection();
                    if (conn.getContentType().indexOf("text/html") == -1) return linkList.iterator();
                    InputStream in = (InputStream)url.getContent();
                    byte text[] = new byte[1024];
                    String buffer = "";
                    do
                    {
                        int start, start2, stop, stop2;
                        if (pageSize <= 0) { break;}
                        int len = in.read(text,0,Math.min(1024,pageSize));
                        if (len == -1) break;
                        pageSize -= len;
                        buffer = buffer + new String(text,0,len);
                        do {
                            start = buffer.indexOf("href=\"");
                            start2 = buffer.indexOf("HREF=\"");
                            if (start == -1) start = start2;
                            if (start2 != -1 && start2 < start) start = start2;
                            if (start == -1) {
                                if (buffer.length() >= 6) buffer = buffer.substring(buffer.length()-6);
                                else buffer = "";
                                break;
                            }
                            start = start + 6;
                            stop = buffer.indexOf("\"",start);
                            stop2 = buffer.indexOf('>',start);
                            if (stop == -1) stop = stop2;
                            if (stop2 != -1 && stop2 < stop) stop = stop2;
                            stop2 = buffer.indexOf('#',start);
                            if (stop == -1) stop = stop2;
                            if (stop2 != -1 && stop2 < stop) stop = stop2;
                            if (stop == -1)
                            {
                                buffer = buffer.substring(start-6);
                                break;
                            }
                            String newLink = buffer.substring(start,stop);
                            URL linkURL = new URL(url,newLink);
                            String name = linkURL.toExternalForm();
                            if (name.startsWith("http:"))
                            {
                                linkList.add(name);
                            }
                            buffer = buffer.substring(stop+1);
                        } while (true);
                    } while (true);
                    in.close();
                } catch (Exception e) {};
            }
        }
        return linkList.iterator();
    }

    public static void main(String args[])
    {
	// take in the input from the terminal window
	boolean takeInput = false; 
	Scanner input = new Scanner(System.in);
	String mainPage = "";
	String searchPage = "";

	// if the input is not a valid page then ask for another page
	while(!takeInput){
	    takeInput = true;     
	    System.out.println("Enter the URL of the  page you want to search in :");
	    mainPage = input.next();
	    if(!mainPage.startsWith("http://")){
		System.out.println("Invalid Page, try Again!");
		takeInput = false; 
	    }
	}
	
	takeInput = false;

	// if the input page to be searched is not a valid  page then ask for another page
	while(!takeInput){
	    takeInput = true;     
	    System.out.println("Enter the URL of the  page you want to search inside " + mainPage+" :");
	    searchPage = input.next();
	    if(!searchPage.startsWith("http://")){
		System.out.println("Invalid Page, try Again!");
		takeInput = false; 
	    }
	}
	
	//main webpage
        HTML page = new HTML(mainPage,20*1024);

	// create an association of the page and Ineger and enqueue it on a queue. 
	Association <HTML,Integer> assoc = new Association<HTML,Integer> (page, 0);
	QueueList <Association<HTML,Integer>> web = new QueueList <Association<HTML,Integer>>();
	web.enqueue(assoc);
	
	// webpage to be searched
	HTML findPage = new HTML(searchPage,20*1024);
	
	//calls search method which will either return -1 or a positive integer, indicating the distance form the main
	//page
	int distance = search (findPage, web);
	
	
	// if the distance returned by search is -1 the the page cannot be found
	// if it is 0 or a positive integer, then it print the distance between the pages.
	if (distance == -1){
	    System.out.println(findPage + " cannot be found");    
	}else{
	    System.out.println("The distance from "+ page +" to " + findPage +" is :" + distance);
	}
	
    }
    

    // takes the page to be explored, a queue where the contents of the page to be added and the value for the
    // distance from the page
    public static void explore (HTML page, QueueList list, Integer value){
	// while page has content, create an association of the page and the distance from the main page
	// and add to the list
	while(page.hasNext()){
	    HTML newPage = new HTML(page.nextURL(),20*1024);
	    Association <HTML,Integer> assoc = new Association <HTML,Integer> (newPage, value);
	    list.enqueue(assoc);
	} 
    }
    
    // search takes the page to be searched for and a queue where the associations of the pages and its distances from
    // the  main page is added. 
    public static int search (HTML find, QueueList list){
	// until the list is empty, dequeue the tail of queue and check if it is the page we are looking for. 
	// if it is the page, return the distance. 
	// otherwise explore the dequeued tail, the explore will add the contents of the dequeued page at the head of
	// the queue. 
	// if no value is returned and the queue is empty, then return -1; 
	while (!list.isEmpty()){
	    Association <HTML,Integer> assoc = (Association<HTML,Integer>)list.dequeue();
	    if (assoc.getKey().equals((HTML)find)){
		return assoc.getValue();
	    }  
	    explore(assoc.getKey(),list, assoc.getValue()+1);
	}
	return -1; 
    }
}
