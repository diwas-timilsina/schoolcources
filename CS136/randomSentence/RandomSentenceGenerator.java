
import structure5.*;
import java.util.Random;
import java.util.Scanner;

/**
 * Diwas Timilsina
 *
 *  28th March 2013
 *
 * A Random Sentence Generator
 * 
 * This class currently parses a grammar file from standard input.
 * Your job is to extend it to print random sentences from the
 * grammar.
 */
public class RandomSentenceGenerator {

    /** The grammar for the generator */
    protected DefinitionTable grammar;

    /**
     * Create a new grammar from the given file.
     */
    public RandomSentenceGenerator(Scanner in) {
	grammar = new DefinitionTable();
	readGrammar(in);
    }


    /**
     * post: reads one production and returns it <p> 
     * 
     * Student: Change this to parse and create a production properly.
     */
    protected Vector<String> readOneProduction(Scanner in) {
	
	Vector <String> vect = new Vector<String> (); 
	while (true) {
	    String token = in.next();
	    if (token.equals(";")) break;
	    vect.add(token);
	    Assert.condition(in.hasNext(),"End of File occured while parsing production");
	}
	return vect;
    }
    
    /**
     * post: reads the name and productions for one non-terminal <p>
     * Student: Change this to parse the productions, create a
     *          definition and add it to grammar
     */
    protected void readOneNonTerminalDef(Scanner in) {

	String name = in.next();  // the non-terminal name
	Definition def = new Definition();

	Assert.condition(in.hasNext(), 
			 "End of File occured while parsing non-terminal " 
			 + name);
	Assert.condition(in.next().equals("{"), 
			 "Expected { for " + name);
	while (true) {
	    Assert.condition(in.hasNext(), 
			     "Are you missing a } at end of " + name + "}");

	    // if we see a } as the next token, break out of the loop,
	    // because we will be done reading productions.
	    if (in.hasNext("}")) break;
	    
	    // read one production of the defination and then add to the grammar table	
	    Vector <String> singleProduction = readOneProduction(in);
	    def.add(singleProduction); 
	}
	// read the "}" at the end of the definition.
	Assert.condition(in.next().equals("}"), "Error reading } for " + name);
	
	// adds to the grammar
	grammar.add(name,def);
    }


    /**
     * Read a grammar from a file. <p>
     */
    protected void readGrammar(Scanner in) {
	while (in.hasNext()) {
	    readOneNonTerminalDef(in);
	}
    }

    /**
     * Print out the grammar.  Useful for debugging.
     */
    public String toString() {
	return grammar.toString();
    }


    /**
     * methods to generate random sentences from the
     * grammar.
     */ 
    
    public void expandWord(String str){
	if (grammar.contains(str)){
	    Definition def = grammar.get(str);
	    Vector<String> vect = def.random();  
	    String feed = "";
	    for(int i = 0; i < vect.size(); i++){
		feed = vect.get(i);
		expandWord(feed);
	    } 
	} else {
	    char ch = str.charAt(0);
	    if (Character.isLetterOrDigit(ch)){
		System.out.print(str + " ");	
	    }else{
		System.out.print(str);
	    } 
	}
    }
    
   
    public static void main(String args[]) {
	RandomSentenceGenerator rsg = 
	    new RandomSentenceGenerator(new Scanner(System.in));

	System.out.println("\n");
	// create three random expressions and exit
	int i = 0;
	while ( i < 3) {
	    rsg.expandWord("<start>");
	    i++;
	}
	System.out.println("\n");	
    }
}
