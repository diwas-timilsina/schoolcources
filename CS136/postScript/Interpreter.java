import structure5.*;

/*
 * Diwas Timilsina
 *
 * A class that takes input from the user and uses stacks to perform mathematical
 * operations.
 */

public class Interpreter {

    // The stack that the information is stored in    
    private static StackList <Token> list;
    
    // The constructor for the Interpreter that creates a new stack.
    public Interpreter(){
	list = new StackList<Token>();
    }
    
    // Creates an instance of the interpreter, a table and a reader.
    // Tells the interpreter to interpret the input.
    public static void main (String args[]){
	Interpreter inter = new Interpreter();
	Reader read = new Reader();
	SymbolTable table = new SymbolTable();
	inter.interpret(read,table);
    }
    
    // Tells what to do with the input from the reader.
    private void interpret(Reader read, SymbolTable table){
	Token t;

	// It keeps interpreting as long as there is input/
	while(read.hasNext()){
	    t = read.next();

	    // If the token is a symbol, it checks the if statements.
	    if(t.isSymbol()){

		// Quit exits the program.
		if(t.getSymbol().equals("quit")) break;
		
		// Pop pops the token off the stack.
		else if (t.getSymbol().equals("pop")){
		    Assert.pre(list.size() != 0,"No element to remove");
		    list.pop();
		}
		
		// pstack calls pstack, which prints the stack.
		else if (t.getSymbol().equals("pstack")){
		    this.pstack();
		}
		
		// calls the add method
		else if (t.getSymbol().equals("add")){
		    this.add();
		}
		
		// calls the sub method
		else if (t.getSymbol().equals("sub")){
		    this.sub();
		}
		
		// calls the mul method
		else if (t.getSymbol().equals("mul")){
		    this.mul();
		} 
		
		// calls the div method
		else if (t.getSymbol().equals("div")){
		    this.div();
		} 
		
		// calls the dup method
		else if(t.getSymbol().equals("dup")){
		    this.dup();
		}
		
		// calls the exch method
		else if(t.getSymbol().equals("exch")){
		    this.exch();
		}
		
		// calls the eq method
		else if(t.getSymbol().equals("eq")){
		    this.eq();
		}
		
		// calls the ne(not equal) method
		else if(t.getSymbol().equals("ne")){
		    this.ne();
		}
		
		// calls the if operator 
		else if(t.getSymbol().equals("if")){
		    this.ifOperator(table);
		}
		
		// calls the def method
		else if(t.getSymbol().equals("def")){
		    this.def(table);
		} 
		
		// calls the lessThan method
		else if(t.getSymbol().equals("lt")){
		    this.lessThan();
		}
               
		// prints the table
		else if (t.getSymbol().equals("ptable")){
		    System.out.println(table.toString());
		}
		
		// checks if the table contains the symbol 
		else if (table.contains(t.getSymbol())){
		    // If it is a procedure, creates a new list of the procedure
		    // and a new reader that takes that list as the input
		    // calls interpret with the new reader
		    if (table.get(t.getSymbol()).isProcedure()){
			Token tok = table.get(t.getSymbol());
			List<Token> l = tok.getProcedure();
			Reader r = new Reader(l);
			this.interpret(r, table);
		    } else {
			// Otherwise push the token that the symbol represents
			Token to = table.get(t.getSymbol());
			list.push(to);
		    }
		} 
		// push any other symbol onto the stack
		else{
		    list.push(t);
		}
	    } 

	    // If the token is a number or boolean, push onto the stack
	    else if(t.isNumber() || t.isBoolean()){
		list.push(t);
	    }
	    
	    // If the token is a procedure, push it onto the stack
	    else if(t.isProcedure()){
		list.push(t);
	    }
	} 
    }
    
    // Prints the stack
    private void pstack(){
	// creates a temperary stack
        Stack<Token> temp = new StackList<Token>();
	// while the list is not empty, prints the top value, pops it off the list
	// and pushes it onto the temp stack
	while(!list.isEmpty()){
	    Token t = list.peek();
	    System.out.println(t);
	    temp.push(list.pop());
	}
	// pushes the tokens back onto the original stack
	while(!temp.isEmpty()){
	    list.push(temp.pop());
	}	
    }
    
    // Adds the two top values
    // pre: top two tokens are doubles
    // post: pushes double onto stack
    private void add(){ 
	double temp1 = list.peek().getNumber();
	list.pop();
	double temp2 = list.peek().getNumber();
	list.pop();
	Token t = new Token(temp1+temp2);
	list.push(t);
    }
    
    // Subtracts the top number from the one below and pushes the result.
    // pre: top two tokens are doubles
    // post: pushes double onto stack
    private void sub(){ 
	double temp1 = list.peek().getNumber();
	list.pop();
	double temp2 = list.peek().getNumber();
	list.pop();
	Token t = new Token(temp2-temp1);
	list.push(t);
    }
    
    // Multiplies the top two values and pushes the result
    // pre: top two tokens are doubles
    // post: pushes double onto stack
     private void mul(){ 
	 double temp1 = list.peek().getNumber();
	list.pop();
	double temp2 = list.peek().getNumber();
	list.pop();
	Token t = new Token(temp1*temp2);
	list.push(t);
     }
    
    // Divides the number second from the top by the one on top and pushes the 
    // result
    // pre: top two tokens are doubles
    // post: pushes double onto stack
    private void div(){ 
	  double temp1 = list.peek().getNumber();
	list.pop();
	double temp2 = list.peek().getNumber();
	list.pop();
	Token t = new Token(temp2/temp1);
	list.push(t);
      }
    
    // Duplicates the top number
    // pre: top token is double
    // post: token is duplicated
    private void dup(){
	Token t = list.peek();
	list.push(t);
    }

    // Switches the order of the top two numbers
    // pre: top two tokens are doubles
    // post: order is switched
    private void exch(){ 
	Token temp1 = list.peek();
	list.pop();
	Token temp2 = list.peek();
	list.pop();
	list.push(temp1);
	list.push(temp2);
    }

    //  true if the top two numbers are equal and false if they are not and
    // pushes the result
    // pre: top two tokens are doubles
    // post: pushes a boolean onto the stack
    private void eq(){
	Token temp1 = list.peek();
	list.pop();
	Token temp2 = list.peek();
	list.pop();
	boolean bool = temp1.equals(temp2);
	Token t = new Token(bool);
	list.push(t);
    }
    
    // Tests if the top two numbers are not equal and pushes the result
    // pre: top two tokens are doubles
    // post: pushes a boolean onto stack
    private void ne(){
	Token temp1 = list.peek();
	list.pop();
	Token temp2 = list.peek();
	list.pop();
	boolean bool = !(temp1.equals(temp2));
	Token t = new Token(bool);
	list.push(t);
    }
    
    // Adds a symbol and its token value to the symbol table
    private void def(SymbolTable table){
	Token temp = list.peek();
	list.pop();
	String str = list.peek().getSymbol();
	String str2 = str.substring(1); 
	list.pop();
	table.add(str2,temp); 
    }

    // takes a token and a boolean, if the boolean is true, interprets the token
    // pre: top value is token, below is boolean
    private void ifOperator(SymbolTable table){
	Token tok = list.peek();
	list.pop();
	boolean bool = list.peek().getBoolean();
	list.pop();
	
	if(bool){
	    Reader read = new Reader(tok);
	    this.interpret(read, table);
	}	
    }

    // Compares the top two numbers and pushes the boolean value of whether the
    // second from the top is less than the top number
    // pre: the top two tokens are doubles
    // post: pushes a boolean onto the stack
    private void lessThan(){
	double temp1 = list.peek().getNumber();
	list.pop();
	double temp2 = list.peek().getNumber();
	list.pop();
	boolean bool = (temp2 < temp1);
	Token t = new Token(bool);
	list.push(t);
    }
}
