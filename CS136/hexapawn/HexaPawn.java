import structure5.*;
import java.util.Random;
import java.util.Iterator;
import java.util.Scanner;

/**
 * This class is responsible for creating the game and playing the game.
 *
 * Diwas Timilsina
 * 5/9/2013
 */  
public class HexaPawn {

    public static void main (String [] args){
	// row and column for the hexboard
 	int row = Integer.parseInt(args [0]);
	int column = Integer.parseInt(args [1]);
	HexBoard board = new HexBoard (row,column);
	
	// starting player
	char startingPlayer = HexBoard.WHITE;

	// create the game tree
	GameTree game = new GameTree(board, startingPlayer);

	// reading the players
	Player p1 = null;
	Player p2 = null;
	String selectPlayer1 = args[2].toLowerCase();
	String selectPlayer2 = args[3].toLowerCase();
	
	// select the first player from human, comp, or random 
	if(selectPlayer1.equals("human")){
	    p1 = new HumanPlayer(HexBoard.WHITE);
	}else if (selectPlayer1.equals("comp")){
	   
	    // when the player is a computer, play the game
	    // with the random player to learn winning moves
	    p1 = new ComputerPlayer(HexBoard.WHITE);
	    
	    int i = 1;
	    while(i < 10000){
		p1.play(game,new RandomPlayer(HexBoard.WHITE));
		i ++;
	    } 
	    
	}else if (selectPlayer1.equals("random")){
	    p1 = new RandomPlayer(HexBoard.WHITE);
	}else {
	    Assert.fail("Invalid Player Input");
	} 

	//select the second player form human comp, or random 
	if(selectPlayer2.equals("human")){
	    p2 = new HumanPlayer(HexBoard.BLACK);
	}else if (selectPlayer2.equals("comp")){
	    
	     // when the player is a computer, play the game
	    // with the random player to learn winning moves
	    p2 = new ComputerPlayer(HexBoard.BLACK);
	    
	    Player temp = new RandomPlayer(HexBoard.WHITE);
	    int i = 1;
	    while(i < 100){
		
		temp.play(game,new ComputerPlayer(HexBoard.BLACK));
		i ++;
	    } 
	    
	}else if (selectPlayer2.equals("random")){
	    p2 = new RandomPlayer(HexBoard.BLACK);
	}else {
	    Assert.fail("Invalid Player Input");
	} 
	
	//identifies the players
	System.out.println("Player 1 is: " + selectPlayer1 +"  '"+HexBoard.WHITE+"'");
	System.out.println("Player 2 is: " + selectPlayer2 +"  '"+HexBoard.BLACK+"'");
	
	
		
	//play p1 against player 2 and store the returned winner in variable
	//winner 
	Player winner = null;
	
	if (p1 != null && p2 != null){
	     winner = p1.play(game,p2);
	}
	
	// print out the winner
	if (winner == p1){
	    System.out.println("The Winner is: Player 1");
	} else{
	    System.out.println("The Winner is: Player 2");
	}
	
	//identifies the players
	System.out.println("Player 1 is: " + selectPlayer1 +"  '"+HexBoard.WHITE+"'");
	System.out.println("Player 2 is: " + selectPlayer2 +"  '"+HexBoard.BLACK+"'");
	
    }
     
}