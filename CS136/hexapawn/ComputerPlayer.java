
import structure5.*;
import java.util.Random;
import java.util.Iterator;
import java.util.Scanner;

/**
 * This class implement computer player.When playing as a computer, we check to see if the game has already
 * been won by the opponent. if that is the case, the winner is the opponent player. Otherwise, we print   
 * the board, present the possible moves, select a move randomly. If the winner is opponent, then remove the
 * losing node from the tree.
 *
 * Diwas Timilsina
 * 5/9/2013
 */

public class ComputerPlayer implements Player{
    //char to represent the player
    private char player;
    //to randomly select the moves
    private Random ran;
    //to remove the losing node 
    protected boolean trim;
    
    // constructor to initiallize the player, the random generator and the trim variable
    public ComputerPlayer(char color){
	Assert.pre(color == HexBoard.WHITE || color == HexBoard.BLACK,"Invalid Player");	
	player = color;
	ran = new Random();
	trim = false;
    }   
    // plays the game by printing the current board and check to see if 
    // the opponent hasn't already won the game. if the opponent has already won the game,
    // return opponent as a winner. Otherwise, present the possible moves  and randomly 
    //  select a move from all possible moves. Finally, if the opponent was the winner, remove the
    // loosing node from the gameTree.
    public Player play(GameTree node, Player opponent){

	HexBoard board = node.board();
	node.printBoard();
	node.printMoves();
	
	if(node.childSize() == 0){
	    trim = true;
	    return opponent;
	}else if(board.win(HexBoard.opponent(player))){
	    trim = true;
	    return opponent;
	}else{
	    int nextMove = ran.nextInt(node.childSize()); 
	   
	    node = node.getChild(nextMove);
	    Player winner = opponent.play(node,this);  
	   
	    // if opponent is the winner, remove the losing node 
	    if (winner == opponent && trim){
		trim = false;
		node.removeMove(nextMove);
	    }
	    return winner;
	} 
    }

    // play method that doesnot print anything
    public Player notPrintPlay(GameTree node, Player opponent){
	HexBoard board = node.board();
	if(node.childSize() == 0){
	    trim = true;
	    return opponent;
	}else if(board.win(HexBoard.opponent(player))){
	    trim = true;
	    return opponent;
	}else{
	    int nextMove = ran.nextInt(node.childSize()); 
	    node = node.getChild(nextMove);
	    Player winner = opponent.play(node,this);  
	    
	    // if opponent is the winner, remove the losing node 
	    if (winner == opponent && trim){
		trim = false;
		node.removeMove(nextMove);
	    }
	    return winner;
	} 
    } 
}