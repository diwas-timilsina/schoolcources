
import structure5.*;
import java.util.Random;
import java.util.Iterator;
import java.util.Scanner;

/**
 * This class implement random player.When playing as random player, we check to see if the game has already
 * been won by the opponent. if that is the case, the winner is the opponent player. Otherwise, we print   
 * the board, present the possible moves, randomly select a move from the possible set of moves
 * and had off the play to opponent.
 *
 * Diwas Timilsina
 * 5/9/2013
 */

  public class RandomPlayer implements Player{
    //char that represent the player
    private char player;
    //to randomly select a move
    private Random ran;

    //constructor to initialize the player and the random generator
    public RandomPlayer(char color){
    	Assert.pre(color == HexBoard.WHITE || color == HexBoard.BLACK,"Invalid Player");	
	player = color; 
	ran = new Random();
    }

    // plays the game by printing the current board and check to see if 
    // the opponent hasn't already won the game. if the opponent has already won the game,
    // return opponent as a winner. Otherwise, present the possible moves and randomly 
    //  select a move from all possible moves. Finally hand the play to the oppoenent
    public Player play(GameTree node, Player opponent){
        HexBoard board = node.board();
	node.printBoard();
	node.printMoves();
	if(node.childSize() == 0){
	    return opponent;
	}else if(board.win(HexBoard.opponent(player))){
	    return opponent;
	}else{
	    int nextMove = ran.nextInt(node.childSize()); 
	    node = node.getChild(nextMove);
	    return opponent.play(node,this);
	} 
    } 

    //play method without printing anything
    public Player nonPrintPlay(GameTree node, Player opponent){
	HexBoard board = node.board();
	if(node.childSize() == 0){
	    return opponent;
	}else if(board.win(HexBoard.opponent(player))){
	    return opponent;
	}else{
	    int nextMove = ran.nextInt(node.childSize()); 
	    node = node.getChild(nextMove);
	    return opponent.play(node,this);
	}
    }

    // to print the player's character representation
    public String toString(){
	return ""+ player;
    }
    
}