/*
 * Recursion.java
 *
 * [Diwas Timilsina , 21 Feb 2013]
 *
 * Starter code for the recursion lab.
 *
 */
import structure5.*;

public class Recursion {


    /*****  1  ***************************************************/

    /*
     * Return number of cannoballs in pyramid with the given height.
     * 
     * run time analysis: O(n^2)
     * where n is the number of cannon balls. 
     *
     */
    public static int countCannonballs(int height) {
	if (height == 0) {
	    return 0; 
	} else {
	    return (height * height) + countCannonballs(height-1);
	} 
    }


    /*****  2  ***************************************************/

    /*
     * Return true if str is a palindrome.
     *
     * run time analysis: O(n)
     * where n is the length of the given string
     */
    public static boolean isPalindrome(String str) {
	// base case
	if (str.length() < 2){
	    return true;
	// recurisive case
	}else{
	    return str.charAt(0) == str.charAt(str.length()-1) &&
		isPalindrome (str.substring (1,str.length()-1));
        }
    }

    /*****  3  ***************************************************/

    /*
     * Return true if str is a string of matched parens,
     * brackets, and braces.
     *
     * run time analysis: O(n)
     * where n is the length of the given string
     */
    public static boolean isBalanced(String str) {
	if (str.equals("")){
	    return true; 
	}else{ 
            if ( str.indexOf("()") != -1){
		 return isBalanced (str.substring (0, str.indexOf("()")) + 
				    str.substring (str.indexOf("()") + 2));
	    } else if ( str.indexOf("{}") != -1 ){
		return  isBalanced (str.substring (0, str.indexOf("{}")) + 
				    str.substring (str.indexOf("{}")+ 2));
	    } else if ( str.indexOf("[]") != -1){
		return isBalanced (str.substring (0, str.indexOf("[]")) + 
			   str.substring (str.indexOf("[]") + 2));
	    }
        }   
	return false; 
    }


    /*****  4  ***************************************************/

    /*
     * Print all substrings of str.  (Order does not matter.)
     *
     * run time analysis: O (2^n)
     * where n is the length of the string
     */
    public static void printSubstrings(String str) {
	subStringHelper(str,"");
    }
    
    public static void subStringHelper(String str, String soFar){
	
	if (str.length() == 0){
	    System.out.println("The required Subset is: "+ soFar); 
	} else {
	    subStringHelper (str.substring(1), str.substring(0,1) + soFar);
    	    subStringHelper (str.substring(1), soFar);			
	}

    } 


    /*****  5  ***************************************************/

    /*
     * Print number in binary
     *
     * run time analysis: O(n)
     * where n is the number of digits in the given number
     */
    public static void printInBinary(int number) {
	if (number <=  1){
	    System.out.print("The Binary Equvalence is:" + number);
	} else{
            printInBinary(number/2);
	    System.out.print ((number % 2)); 
            
	}

    }


    /*****  6  ***************************************************/

    /*
     * Return whether a subset of the numbers in nums add up to sum,
     * and print them out.
     *
     * run time analysis: O(2^n) 
     * where n is the number of elements in the array nums
     */
    public static boolean printSubSetSum(int nums[], 
					 int targetSum) {

	return printSubSetSumHelper(nums, targetSum, 0 ,"") ;
    }

    public static boolean printSubSetSumHelper(int nums[],int targetSum,
					       int index, String soFar){
	if (nums.length == index){
	    if(targetSum == 0){
		System.out.println ("\n"+ "Part a: The only subset with the target sum is:" + soFar);
		return true;
	    }else{
		return false; 
	    }          
	} else {
	    return printSubSetSumHelper(nums,targetSum - nums[index],
					index+1, soFar + nums[index]+",") 
		|| printSubSetSumHelper(nums,targetSum, index+1, soFar);
	
	}
    }      


    /*
     * Return the number of different ways elements in nums can be
     * added together to equal sum.
     * run time analysis: O(2^n) 
     * where n is the number of elements in the array nums
     */
    public static int countSubSetSumSolutions(int nums[], 
					      int targetSum) {
	
	return countSubSetSumSolutionHelper (nums,targetSum,0, "");

    }


    public static int countSubSetSumSolutionHelper(int nums[],int targetSum,
						   int index, String soFar){
      	if (nums.length == index){
	    if (targetSum ==0){
		System.out.println (" Part b:  The subset with the target sum is: " + soFar);
		return 1; 
	    }else {
		return 0;
	    }
	} else{
	    return countSubSetSumSolutionHelper (nums,targetSum - nums[index],
					 index+1, soFar + nums[index] + ",") +  
		countSubSetSumSolutionHelper (nums,targetSum,index+1, soFar);
	}
     } 	



    /*****  7  ***************************************************/
    
    public static void listCompletions(String digitSequence, 
				       Lexicon lex) {
    
    }


    /**************************************************************/
    
    /*
     * Add testing code to main to demonstrate that each of your 
     * recursive methods works properly.
     */
    public static void main(String args[]) {
	
        // test code for problem 2
	System.out.println(isPalindrome("mom"));
	System.out.println(isPalindrome("cat"));
	System.out.println(isPalindrome("level"));



	// test code for problem 1
		System.out.println(countCannonballs(3));
		System.out.println(countCannonballs(4));
		
	// test code for problem 3
		System.out.println(isBalanced("[(){}]"));

	// test code for problem 4
		printSubstrings("ABC");

      	// test code for problem 5 
		printInBinary(11);
	
	//test code for problem 6
		//first part
		int nums[] = new int[5];
			nums[0] = 1;
			nums[1] = 2;
			nums[2] = 3;
			nums[3] = 4;
			nums[4] = 5;
		    printSubSetSum(nums,5);
		    System.out.println ("The total number of subsets with  targetSum is :" 
					+ countSubSetSumSolutions(nums,5));
    } 
}
