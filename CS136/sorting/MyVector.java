
/* 
 * Diwas Timilsina
 * 3/1/2013 
 * 
 * This class extends the vector class, but adds methods allowing the vector to 
 * be sorted.  It is used to store different information about students. 
 * 
 */ 


import structure5.*; 
import java.util.Comparator; 
import java.util.Scanner; 

public class MyVector<E> extends Vector<E> { 
    
    // The constructor just calls that of the parent class, vector. 
    public MyVector() { 
     	super(); 
    } 
    
    // Sorting the vector calls selection sort on the vector. 
    public void sort(Comparator<E> c){ 
	selectionSort ( c, this.size()-1); 
    } 
    
    // Selection sort uses the specific comparator to sort the vector by finding 
    // the maximum value, based on the comparator, 
    // and putting it in the last index. 
    private void selectionSort(Comparator<E> c, int lastIndex){ 
	if (lastIndex > 0) { 
	    int max = 0; 
	    for (int i = 1; i <= lastIndex; i++) { 
		if (c.compare(this.get(i),this.get(max)) > 0) { 
		    max = i; 
		} 
	    } 
            
	    E tmp = this.get(lastIndex); 
	    this.set(lastIndex,this.get(max)); 
	    this.set(max,tmp); 
	    selectionSort(c, lastIndex-1); 
	} 
    } 
    
    // This adds an address to a vector of associations by going through and 
    // seeing if that address already is in the array. If it is, it increments 
    // the value by 1, otherwise it adds a new association. 
    // pre: can only add to a vector of associations. 
    private void addAddress(MyVector <Association<String,Integer>> address, String add){ 
	if(!add.equals("")){ 
	    Association <String, Integer> assoc; 
	    for (int i = 0; i < address.size(); i++){ 
		assoc = address.get(i); 
		if(assoc.getKey().equals(add)){ 
		    assoc.setValue(assoc.getValue()+1); 
		    return; 
		} 
	    } 
	    Association <String, Integer> otherAssoc = 
		new Association <String, Integer> (add, 1); 
	    address.add(otherAssoc); 
	}
    }
    
    // This runs the program. 
    public static void main(String args[]) { 
	// The scanner reads the input, which is the phone book 
  	Scanner in = new Scanner(System.in); 
	
	// Two new MyVectors are created, one for students, one for addresses 
	MyVector <Student> myVector = new MyVector<Student>(); 
	
	MyVector<Association<String,Integer>> addressVector = 
	    new MyVector<Association<String,Integer>>(); 
	
	
	// Reads the file until the end. 
	while(in.hasNextLine()) { 
	    
	    // The first line is the name 
	    String name = in.nextLine(); 
	    
	    // The second line is the address 
	    String address = in.nextLine(); 
	    
	    // If address is unknown, it becomes an empty string 
	    if (address.equals("UNKNOWN")){ 
		address = ""; 
	    } 
	    
	    // The next line has 3 pieces of information so we have to separate 
	    // it. 
	    String line = in.nextLine(); 
	    
	    // The phone is the first piece. 
	    int firstSpace = line.indexOf(" "); 
	    String phone = line.substring(0,firstSpace); 
	    
	    long phoneInput = 0; 
	    
	    // If we can not parse the phone into a long, we make it -1. 
	    try{ 
		phoneInput = Long.valueOf(phone);  	
	    }catch (Exception e) { 
		phoneInput = -1; 
	    } 
	    
	    // The next piece is the SU box, which we handle the same way. 
	    int secondSpace = line.indexOf(" ", firstSpace + 2); 
	    String box = line.substring(firstSpace + 2, secondSpace); 
	    
	    long boxInput; 
	    
	    try{ 
		boxInput = Long.valueOf(box); 
	    }catch(Exception e){ 
		boxInput = -1; 
	    } 
	    
	    // The third piece is the home phone, which we handle the same way. 
	    String home = line.substring(secondSpace + 2); 
	    
	    long homeInput; 
	    try{ 
		homeInput = Long.valueOf(home); 
	    }catch(Exception e){ 
		homeInput = -1; 
	    } 
	    
	    // The final line is the dashed line that we ignore. 
	    String finalLine = in.nextLine(); 
	    
	    // We add the address to the address vector. 
	    addressVector.addAddress(addressVector, address); 
	    
	    // We create a new student with all of the information and add it to 
	    // the vector of students. 
	    Student s = new Student(name,address,phoneInput,boxInput,homeInput); 
	    myVector.add(s); 
	} 
	
	// answer to question number 1 
	// We use the name comparator to sort the vector alphabetically and 
	// print out the first student. 
	myVector.sort(new NameComparator()); 
	System.out.println("The person who appears first in the alphabetically sorted list is: " 
			   + (myVector.get(0)).getName()); 
	
	// answer to question number 2 
	// We use the SU box comparator to sort the vector and print the student 
	// with the smallest box and the largest box. 
	myVector.sort(new BoxComparator()); 
	for ( int i = 0; i < myVector.size(); i++){ 
	    if ((myVector.get(i)).getBox() != -1){ 
		System.out.println("The person with Smallest SU BOX number is: "    
				   +(myVector.get(i)).getName() + "with SU BOX number " + 
				   (myVector.get(i)).getBox()); 
		break; 
	    } 
	} 
	System.out.println("The person with Largest SU BOX number is: "    
			   +(myVector.get(myVector.size()-1)).getName() + 
			   "with SU BOX number " + 
			   (myVector.get(myVector.size()-1)).getBox()); 
	
	
	//answer to question number 3 
	// We use the highest vowel comparator to print which student has the 
	// most vowels in their name. 
	myVector.sort(new HighestVowel()); 
	System.out.println("The person who has highest vowels in his/her name is: " 
			   + (myVector.get(myVector.size()-1)).getName());     
	
	
	// answer to question number 4 
	// We sort the address vector using the address comparator and we get 
	// the address at the last index, because that is the most common. 
	// Then we search the vector of students and print the names of the 
	// students that have that address. 
	addressVector.sort(new AddressComparator()); 
	Association<String,Integer> a = addressVector.get(addressVector.size()-1); 
	String mostCommon = a.getKey(); 
	System.out.println("The people with the most common address " + mostCommon + 
			   " are: " + "\n"); 
	for(int i = 0; i < myVector.size(); i++){ 
	    if((myVector.get(i).getAddress()).equals(mostCommon)){ 
		System.out.println((myVector.get(i)).getName()); 
	    } 
	}    
    } 
} 

// comparator class to sort people according to address 
// It compares two associations by their values. 
// pre: The associations must have a key that is a string and a value that is 
// and integer. 
class AddressComparator implements Comparator<Association<String,Integer>>{ 
    public int compare(Association<String,Integer> a, 
		       Association<String,Integer> b){ 
	int totalFirst = a.getValue(); 
	int totalSecond = b.getValue(); 
	
	if(totalFirst< totalSecond){ 
	    return -1; 
	} else if (totalFirst > totalSecond){ 
	    return 1; 
	} else{ 
	    return 0; 
	} 
    } 
} 
