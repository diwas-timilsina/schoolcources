/*
 * Diwas Timilsina
 *
 * This class keeps track of the information associated with a student. 
 *
 */

import structure5.*;
import java.util.Comparator;


public class Student {
    
    // Instance variables for the differents parts of the student
    String name;
    String address;
    long phone;
    long box;
    long home;
    
    
    // The constructor which takes strings for the name and address and longs
    // for the phone, box, and home phone.
    public Student(String name1, String address1, long phone1, long box1,
		   long home1){
	// Sets the instance variables to the values provided in the constructor
	name = name1;
	address = address1;
	phone = phone1;
	box = box1;
	home = home1;
	
    }
    
    // Returns the name
    public String getName(){
        return name;
    }
    
    // Returns the address
    public String getAddress(){
	return address;
    }
    
    // Returns the phone number
    public long getPhone (){
	return phone;
    }
    
    // Returns the box number
    public long getBox (){
 	return box;
    }
    
    // Returns the home phone number
    public long getHome (){
	return home;
    }
    
    
    
    // A to string method that puts all of the components into a string.
    public String toString() {
	return name + "\n" + address + "\n" + phone + " " + box + " " + home
	    + "\n" + "\n";
    }
}

// Compares names based on alphabetical order of the Strings.
class NameComparator implements Comparator <Student> {
    public int compare(Student a, Student b){
	return a.getName().compareTo(b.getName());
    }
}

// Compares the box numbers by comparing the integers.
// pre: has to be given two students
class BoxComparator implements Comparator <Student> {
    public int compare(Student a, Student b){
	long n1 =  a.getBox();
	long n2 =  b.getBox();
	
	if (n1 < n2){
	    return -1;
	} else if (n1 > n2){
	    return 1;
	} else{
	    return 0; 
	}
    }	
}

// Compares the number of vowels in a name by comparing the total number.
// pre: have to be given two students
class HighestVowel implements Comparator <Student> {
    public int compare(Student a, Student b){
	String first = a.getName();
	String second = b.getName();
	int totalFirst = howManyVowels (first);
	int totalSecond= howManyVowels (second);
	
	if (totalFirst < totalSecond){
	    return -1;
	} else if (totalFirst > totalSecond){
	    return 1;
	} else{
	    return 0; 
	}
    }
    
    // Searches through the string(name) and counting the number of vowels.
    private int howManyVowels (String str){
	int totalVowel = 0;
	for (int i = 0; i < str.length(); i++){
	    if(str.charAt(i) == 'a' || str.charAt(i) == 'e' ||
	       str.charAt(i) == 'i' || str.charAt(i) == 'o' ||
	       str.charAt(i) == 'u'){ 
		totalVowel++;
	    }
	}
	return totalVowel; 
    }
    
}
