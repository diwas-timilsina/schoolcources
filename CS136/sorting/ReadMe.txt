Alexis Savery and Diwas Timilsina
CS 136
Lab 4
------------------------------------------------------------------------------------------
Warm up Questions:

1. 5.5) The runtime of matrix multiplication is O(n^3).

2. 5.23)  Proof by induction
	P(n) = (5^n - 4n - 1) %16 = 0
Base Case: when n=0
	= (5^0 - 4(0) - 1) %16
	= (1-1) %16
	= 0%16=0
Assume true for all values less than n, including n-1.
	5^(n-1) -4(n-1) - 1
Multiply by 5 to get
	5^n - 20(n-1) - 5
	which is also divisible by 16
Add 16(n-1) which is a multiple of 16.
	5^n - 20(n-1)+16(n-1) - 5
	=5^n - 4(n-1) -5
	= 5^n - 4n +4 -5
	= 5^n - 4n -1
So we see that 5^n - 4n - 1 is divisible by 16.

3. 5.26)	n
	?   2i = n(n+1)
	i=1
Base Case: when n = 1
	1
	? 2i = 1(1+1) = 2
	i=1
Inductive Hypothesis:  Assume for all k less than n that 
	k
	? 2i = k(k+1)
	i=1
Proof:	n
	? 2i = n(n+1)
	i = 1
	n-1
	? 2i +2n = n(n+1)
	i=1
	
	(n-1)(n-1+1) +2n = n(n+1)
	(n-1)n +2n = n(n+1)
	n^2 - n + 2n = n(n+1)
	n^2 +n = n(n+1)
	n(n+1) = n(n+1) 

4. 6.3)  In the best case and the average case, it will not require any of the elements to be changed but still need to make comparisons so it will always be O(n^2). 

5. 6.4)  In the worst, best and average cases, the bubble sort has a run time of O(n^2).

Java Files:
In the first java file, there is the MyVector class and the main method that allows the file to run with vectors being constructed.  It provides methods for sorting the vector and also includes the address comparator.
The second java file is the Student class which keeps track of the information of a single student.  It also contains the rest of the comparators.
-------------------------------------------------------------------------------------------------
Thought Questions:
1.  When you construct the vector, it will sort the integers in the reverse order because the RevComparator returns the opposite of whatever comparator it is given as a parameter which happens to be an integer comparator.  So when the vector is sorted, it will put the larger integers first.

2.  You could add parameters to the constructor to give extra information.  Just like in the previous question, how the comparator took another comparator as a parameter, a comparator could take information as a parameter and than use if and else statements to specify how to compare things differently in ascending and descending orders.
------------------------------------------------------------------------------------------------
 The following are the answers to the four questions about the data:

a) The person who appears first in the alphabetically sorted list is: Aaron D. Willey 

b) The person with Smallest SU BOX number is: Brandon P. Abasolo with SU BOX number 1000
   The person with Largest SU BOX number is: Daniel M. Zilkha with SU BOX number 3207

c) The person who has highest vowels in his/her name is: Precious Jivinali Tobias Chimbamba		

d) The people with the most common address 1065 Main  Street are: 

Chandaly Seng
Hayk Ayvazyan
Likea Hor
Clement Ncuti
Thi Kim Dung Pham
Quratul Ain
Jay Gbleh-bo Brown
Fuad Mammadov
Philton Makena
Thuan Bich Nguyen
Derek Brian Apell
Nikoloz Gagua
Mohammed Ali Malfi
Chandra Neyour Clark
Muhammetdurdy Agayev
Tilekbek Shamurzaev
Kavitha Arunasalam
Sarah Elias Bou Atmeh
Donalyn Ursula Minimo
Mariama Cire Sylla
Mohammed Yousef Saleh
Saleh Fadhl Hussein Saleh
Giorgi Mamatelashvili
Alex Alonso Contreras Miranda
Carmen Maria Ruiz Miranda
Edson Bartolomeu Mangunhane
Waheedullah Farooqi Zargar
Maria Jose Sobalvarro Obregon
Precious Jivinali Tobias Chimbamba
------------------------------------------------------------------	
