import structure5.*; 
import java.util.Random;

/*
 * This class is responsible for maintaining a vector of associations in which 
 * a string is the key and its frequency as its value; 
 */
public class FrequencyList{

    // vector of association 
    protected Vector<Association<String, Integer>> wordList; 
    
    // total number of character
    int total; 
   
    // random generator 
    Random random = new Random(); 
   
    // constructor for the class
    public FrequencyList (){
        
	wordList = new Vector <Association <String,Integer>>();     
    }

   
    // post: updates the frequency of the given string if the string is 
    //      alrady there. if the word is new, creates the association of the
    //      string.  
    public void add (String ch){
	total++;
	Association<String,Integer> assoc;
	for (int i = 0; i< wordList.size(); i++){
	    assoc = wordList.get(i);  

         if (assoc.getKey().equals(ch)){
            assoc.setValue (assoc.getValue() + 1);
            return; 
        }          
        }  

        Association<String, Integer> accos =
	    new Association<String, Integer> (ch, 1);
        wordList.add(accos);  
        total++;
      }
   
    
    // to print out the  string 
    public String toString(){
        String result = "";
	for ( int i =0 ; i < wordList.size(); i++) {
            Association < String, Integer> temp = wordList.get(i); 
	    result = result + temp.getKey()+" appears  "+ temp.getValue()+"\n";
        }   
        return result; 

   }

 
    // post: the method will return the string that appeared with the highest
    //       probablity 
     public String pickNext(){
	int n = random.nextInt(total); 
        int j = 0; 
        Association <String,Integer> assoc;
	for (int i=0; i< wordList.size();i++){
	    assoc = wordList.get(i);
            if(j >= n ){
		return assoc.getKey();
            }
             j = j + assoc.getValue();
        }
        return "";   
    }  
}


  
