import structure5.*;
import java.util.Random;
import java.util.Scanner; 

// Diwas Timilsina
// Lab 2
// 14 Feb 2012 

//short answers
// 3.2. add(v) method adds an object v at the end of the vector whereas add(i,v)
//    adds an object v at the position i 
// 3.3. add (i,v) is used to add a new object at the index i
//    set (i,v) it replaces the value at the index i with object v and returs 
//    the object that is just replaced. 
// 3.4. remove (v) is used to remove an element equal to v
//      remove (i) it replaces the element at the index i
// 3.6. Primary advantage of using vectors over array is that vectors are 
//      extensible whereas arrays have fixed length    



// This class is responsible for reading in the input,bulding the table, 
// and printing randomly generated string based on the character sequence 
// probablities of the given text. This class will also contain the static 
// method. 

public class WordGen{

    public static void main (String args []){
       
	// takes the first element of the array args. if nothing is given as 
        // an input,terminates the program. If the some numeric input is 
	// there, asks for the input of a string. Adds the string to a
        // table and then randomly generates texts based on the table   
	
        if (args.length == 0){
	    System.out.println("Usage:java WordGen<level>");
	}else{
          int  k = Integer.parseInt(args[0]); 
        
          // takes the entire string line by line and concatenates them
          // to form a string called text 
          Scanner in = new Scanner(System.in);
	  StringBuffer textBuffer = new StringBuffer();
	  while(in.hasNextLine()){
	     String line = in.nextLine();
	     textBuffer.append(line);
	     textBuffer.append("\n");
	 }  
         String text = textBuffer.toString();
         
         // creates table and add the strings to the table
         Table arrange = new Table();
	 int start = 0;
	 int end = k+1;
	   
         while (end < text.length()){
	 arrange.add(text.substring(start,end)); 
	 start++;
	 end++; 
         } 
        
         // generates random strings based on the table
         int begin = 0; 
         String sub = text.substring (begin, k);
         String next = arrange.pickNext(sub);           
         String result = sub + next; 
         while (begin < 10000){
	     sub = result.substring(begin+1, begin+k+1);
             result = result + arrange.pickNext(sub); 
             begin++;      
         }
 	 System.out.println(result); 

         }  
     }
} 
