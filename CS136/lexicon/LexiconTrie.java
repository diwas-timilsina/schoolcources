import structure5.*; 
import java.util.Iterator; 
import java.util.Scanner; 


/* 
 * Diwas Timilsina 
 * 
 * This class is a tree of lexicon nodes representing letters.  It the branches of the trie represent words. 
 * Methods are available to check for words, adding words, removing words, and contains prefix. 
 * 
 */ 


/**Thought question
 *
 * if we use a ordered vector instead of a trie for sorting out lexicon, it would be more efficient. instead of 
 * making multiple recrusive calls, we can just traverse across the orderedVector. The search method would take
 * a node and index of the node as a parameter. Then we start with the root node with index 0. We then check for
 * all the children, which will be next to each other, by traversing from the index 1 to 1 + index equal to the no of children
 * of the node. Suppose the first character is A, we look for 'A' in among the children of the root node 
 * then after we find the 'A' child, we the jump to the index of the 
 * children of child A(given by 2*index of parent + 1). and repeat the search process again for next character. 
 * if, during this process, we do not find the character we are looking for the  we return the strings with all 
 * the children that are words of the previous node. 
 */


public class LexiconTrie implements Lexicon { 
    // Instance variables representing the root and word count 
    LexiconNode root;   
    int wordCount;   
    
    // Constructor for the trie 
    public LexiconTrie(){ 
	// this needs to be true for the base case of remove method.   
	root = new LexiconNode(' ', true); 
	wordCount = 0; 
    } 
    
   
    // Checks if the trie contains a word 
    public boolean containsWord (String word){ 
	// starts at the root and keeps looking for the next letter of the word in the current node's children 
	LexiconNode finger = this.root; 
	for (int i =  0; i< word.length(); i++){ 
	    LexiconNode child = finger.getChild(word.charAt(i)); 
	    // If there is no child that matches the letter, then return false 
	    if(child == null){ 
		return false;   
	    }else{ 
		finger = child; 
	    } 
	} 
	// If all of the letters in the word are there, return whether the last node has isWord equal to true 
	return finger.isWord(); 
    }   
    
    // Checks if the trie contains a prefix 
    public boolean containsPrefix(String preFix){ 
	// Searches the trie the same way as contains word 
	LexiconNode finger = this.root; 
	for (int i =  0; i< preFix.length(); i++){ 
	    LexiconNode child = finger.getChild(preFix.charAt(i)); 
	    if(child == null){ 
		return false;   
	    }else{ 
		finger = child; 
	    } 
	} 
	// If all of the letters of the prefix are there, return true 
	return true; 
    } 
   
    // Adds a word to the trie 
    public boolean addWord(String inputWord) { 
	// Makes the input all lower case letters 
	String word = inputWord.toLowerCase(); 
	
	// If the word is already in the trie, return false 
	if (containsWord(word)){ 
	    return false; 
	}else{ 
	    // For each letter of the word, it looks for a node whose letter matches it 
	    LexiconNode finger = this.root; 
	    for (int i = 0; i < word.length(); i++){ 
		LexiconNode child = finger.getChild(word.charAt(i)); 
		// If the node exists, then the finger now will point to that node 
		if ( child !=null){ 
		    finger = child ; 
		} else { 
		    // Otherwise create a new node, add that node as a child to the current node, then make 
		    // the new node be the current node 
		    LexiconNode ln = new LexiconNode(word.charAt(i),false); 
		    finger.addChild(ln); 
		    finger = ln;   
		}   
	    }   
	    // Increase word count and set the boolean of the last node to be true 
	    wordCount++; 
	    return finger.setWord(true); 
	} 
    } 
    
     
    // Adds words to the trie from a word file 
    public int addWordsFromFile(String filename) { 
	int count = 0;   
	Scanner in = new Scanner(new FileStream(filename)); 
	// While there is a next word, add the word and increase the count 
	while(in.hasNextLine()){ 
	    String word = in.nextLine(); 
	    addWord(word); 
	    count++; 
	}    
	// return the count 
	return count; 
    } 
    
    // Removes a word from the trie 
    public boolean removeWord(String word) { 
	// Calls a helper method that checks if the word is in the trie and sets the isWord value 
	// of the last node to false 
	boolean bool = remove(this.root,word,0); 
	// If the word is in the trie, it subtracts 1 from the word count and calls another remove method 
	if ( bool ){ 
	    wordCount--; 
	    newRemove(this.root,null, word, 0); 
	} 
	// returns the boolean 
	return bool;	
    } 
    
    // A private method that removes the word from the trie 
    private void newRemove(LexiconNode current, LexiconNode previous, String word, int count){ 
	// If the count is equal to the word lenth, and the current node has no children, and the 
	// current node is not the end of a word, remove it as a child of the previous node 
	if (count == word.length() && current.childSize()==0 && !current.isWord()){   
	    previous.removeChild(current.letter()); 
	    newRemove(this.root,null,word.substring(0,word.length()-1),0); 
	    
	    // If the count is equal to the word length and the current node has children, or if it is a 
	    // word, do nothing 
	} else if (count == word.length() && (current.childSize() != 0 ||current.isWord())) { 
	    return;	      
	} else { 
	    // Otherwise call the method with the current becoming the previous and its child becoming the 
	    // new current nodes 
	    LexiconNode child = current.getChild(word.charAt(count)); 
	    previous = current; 
	    current = child; 
	    newRemove(child,previous,word,count+1); 
	} 
    } 
    
    // A private method that tests if the word is in the trie 
    private boolean remove(LexiconNode pointer, String word, int count){ 
	// If the entire word is in the trie, set the isWord boolean of the last node 
	// to false and return true 
	if ((count)== word.length()){ 
	    pointer.setWord(false); 
	    return true; 
	}else{ 
	    // Get the child of the pointer that corresponds to the next letter in the word 
	    LexiconNode child = pointer.getChild(word.charAt(count)); 
	    // If it doesn't exist, return false 
	    if ( child == null){ 
		return false; 
	    } 
	    // Otherwise, call the method on the rest of the word 
	    return remove(child,word,count+1);   
	} 
    } 
    
    // Returns the word count 
    public int numWords() { 
	// If the word count is less than 0, just return 0 
	if (wordCount < 0){ 
	    wordCount = 0; 
	} 
	return wordCount; 
    } 
       
    // An iterator 
    public Iterator<String> iterator(){ 
	// Creates a queue List of strings 
	QueueList<String> queue = new QueueList<String>(); 
	// Creates a string that contains the root letter 
	String str = " " + root.letter(); 
	// Makes queue equal to the search of the root, queue and string 
	queue = search(root, queue,str); 
	// Returns an iterator of queue 
	return queue.iterator(); 
    } 
    
     
    // Uses the lexicon node iterator to add words to a queue list 
    public QueueList<String> search (LexiconNode finger, QueueList<String> list, String str){ 
	Iterator ite = finger.iterator(); 
	// While there are lexicon nodes 
	while(ite.hasNext()){ 
	    // the child is the next lexicon node, if it is a word, enqueue the string with that child's letter 
	    // at the end 
	    LexiconNode child =(LexiconNode)ite.next(); 
	    if (child.isWord()){ 
		list.enqueue(str + child.letter()); 
	    } 
	    // Search again with the finger now on the child and with the new string 
	    search(child,list, str + child.letter()); 
	} 
	// Return the list 
	return list; 
    } 
    
    // Suggests possible corrections to a target word, within the max distance of letters 
    public SetVector<String> suggestCorrections(String target, int maxDistance) { 
	// Creates a new vector that will hold the possible correction 
	SetVector<String> data = new SetVector<String>(); 
	// Starts with root letter 
	String str = " " + root.letter(); 
	// Calls search method 
	data = search(root,data,str,0,maxDistance,target); 
	
	// Returns the vector of possibilities 
	return data; 
    } 
     
    
    // Searches for possible corrections to the target word and adds them to a vector of words 
    public SetVector<String>  search (LexiconNode node, SetVector<String> data, 
				      String soFar,int count, int dis, String target){ 
	// If the count equals the target length, the last node is the last node of a word, and the 
	// distance has been reduced to 0, add the soFar string to the vector 
	if (count == target.length()){ 
	    if (node.isWord() && dis == 0){ 
		data.add(soFar); 
	    } 
	}else{ 
	    // Iterates through the nodes 
	    Iterator ite = node.iterator(); 
	    while(ite.hasNext()){ 
		LexiconNode child = (LexiconNode)ite.next(); 
		// If the child letter is equal to the target word letter at the count, add the child's 
		// letter to soFar, increment count, and search again 
		if(child.letter() == target.charAt(count)){ 
		    search(child,data,soFar+child.letter(),count+1,dis,target); 
		}     
		// Otherwise, search again, adding the letter and incrementing count, but subtract 
		// 1 from the distance, because the letter is not in the target word 
		search (child, data,soFar+child.letter(),count+1,dis-1,target);   
	    } 
	} 
	// Returns the vector of suggestions 
	return data; 
    } 
    
    // Looks for words that fit the regular expression 
    public SetVector<String> matchRegex(String pattern){ 
	SetVector<String> data = new SetVector<String>(); 
	String soFar = " " + root.letter(); 
	// Calls the search helper method 
	data = searchHelper(root,data,soFar,pattern); 
	return data;	
    } 
    
    // Searches for words that fit the regular expression pattern 
    public SetVector<String> searchHelper(LexiconNode node, SetVector<String> data, 
					  String soFar,String pattern){ 
	// If the pattern has been gone through completely and the last node has isWord true, add that to 
	// the vector of words 
	if (pattern.length() == 0){ 
	    if(node.isWord()){ 
		data.add(soFar); 
	    } 
	}else{ 
	    // If the first character is * or ?, search again, taking that character off of the pattern 
	    // substring 
	    if (pattern.charAt(0) == '*' || pattern.charAt(0) == '?'){ 
		searchHelper(node, data, soFar, pattern.substring(1,pattern.length())); 
	    } 
	    
	    // Iterate through the nodes 
	    Iterator ite = node.iterator(); 
	    while (ite.hasNext()){ 
		LexiconNode child = (LexiconNode)ite.next(); 
		// if the pattern starts with a *, add the child's letter to soFar, and use child as the node 
		if (pattern.charAt(0) == '*'){ 
		    searchHelper(child, data, soFar + child.letter(), pattern); 
		    
		    // if the pattern starts with ?, search again with a substring of the pattern that 
		    // excludes ? and adding the letter to soFar 
		} else if (pattern.charAt(0) == '?'){ 
		    searchHelper(child, data, soFar + child.letter(),pattern.substring(1,pattern.length())); 
		}else{ 
		    // Otherwise, if the letter of the child matches the first character of the pattern, 
		    // add the letter to soFar and search again with the rest of the pattern 
		    if (child.letter() == pattern.charAt(0)){ 
			searchHelper(child,data, soFar + child.letter(), pattern.substring(1,pattern.length())); 
		    } 
		} 
	    } 
	    
	}   
	// Return the vector of words that fit the pattern 
	return data; 
    } 
} 