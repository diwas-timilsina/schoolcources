import structure5.*;
import java.util.Iterator;

/*
 * Diwas Timilsina
 *
 * This class provides the methods and constructor for a single node in the 
 * lexicon trie.  The lexicon node represents a letter, but also stores all
 * of the letters that come after that letter, as its children.
 *
 */

public class LexiconNode implements Comparable{
    // Instance variable representing the letter of the node
    char letter; 
    
    // A boolean representing whether the node represents the last letter of a 
    // word 
    boolean isWord;

    // The vector of the children
    Vector<LexiconNode> pointer;

    // The iterator
    Iterator theIterator;  
    
    // The constructor that takes a letter and boolean as parameters
    public LexiconNode(char aLetter, boolean bool){
	// Sets the letter to be the one passed through and isWord to be the
	// boolean passed through
	letter = aLetter; 
	isWord = bool;
	
	// Creates the vector and the iterator
	pointer = new Vector <LexiconNode>();
	theIterator = (Iterator)iterator();  
    }  
    
    // Allows the nodes to be compared
    public int compareTo (Object obj){
	// Subtracts the letter that the node is being compared to from
	// the letter of the node and returns the result
	return letter - ((LexiconNode)obj).letter();
    }
    
    // Returns the letter of the node
    public char letter(){
	return letter; 
    }
    
    // Adds a child to the vector of children
    public void addChild (LexiconNode ln){
	// Gets the letter of the child, locates the appropriate index, then 
	// adds the child to the vector
	char aLetter = ln.letter();
	int position = locate(aLetter);
	pointer.add(position,ln);
    }
    
    // Uses a binary search to find the index of a letter, or the index that
    // the letter should be at in the vector
    private int locate(char letter){
	int low = 0; 
	int high = pointer.size();
	int mid = (low+high)/2;
	char  midValue;
	while(low<high){
	    midValue = (pointer.get(mid)).letter();
	    if((midValue - letter) < 0){
		low = mid+1;
	    }else{
		high = low;
	    }
	    mid = (low + high)/2;
	}
	return low;
    }
    
    // Gets the child node that has the specified letter
    public LexiconNode getChild(char ch){
	// Compares each node's letter in the vector to the specified letter
	for(int i  = 0; i < pointer.size(); i++){
	    char letter = pointer.get(i).letter();
	    // If they are equal, it returns that node
	    if(letter == ch){
		return pointer.get(i);
	    } 
	}
	// If the letter is not in the vector, returns null
	return null; 
    }
    
    // Removes the child
    public void removeChild(char ch){
	// Gets the child using getChild, then removes it from the vector
	pointer.remove(getChild(ch));
    }
    
    // A lexicon node iterator that returns the iterator for the vector of
    // children
    public Iterator<LexiconNode> iterator(){
	return pointer.iterator();
    }
    
    // Returns the boolean isWord
    public boolean isWord(){
	return isWord;
    }
    
    // Changes the boolean isWord to be the boolean passed in
    public boolean setWord(boolean bool){
	isWord = bool;
	return isWord; 
    }
    
    // Returns the number of children, which is equal to the size of the vector
    public int childSize(){
	return  pointer.size();
    }   
}