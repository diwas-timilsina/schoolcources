
import structure5.*;
import java.util.Random;
import java.io.*;

/**

----------------------------
   Diwas Timilsina
   CS 136 lab 
   29 April 2013
------------------------------- 


 * This class controls the simulation.  The design is entirely up to
 * you.  You should include a main method that takes the array of
 * species file names passed in and populates a world with species of
 * each type.  
 * <p>
 * Be sure to call the WorldMap.pause() method every time
 * through the main simulation loop or else the simulation will be too fast. 
 * For example:
 * <pre>
 *   public void simulate() {
 *       for (int rounds = 0; rounds < numRounds; rounds++) {
 *         giveEachCreatureOneTurn();
 *         pause(100);
 *       }
 *   }
 * </pre>
 */
class Darwin {

    /**
     * The array passed into main will include the arguments that
     * appeared on the command line.  For example, running "java
     * Darwin Hop.txt Rover.txt" will call the main method with s
     * being an array of two strings: "Hop.txt" and "Rover.txt".
     */
    // vector to store the creatures
    private Vector <Creature> creatures;     
    
    // constructor that saves the vector as an instant variable
    public Darwin( Vector<Creature> data){
	creatures = data;
    }

    // main method that runs the game
    public static void main(String s[]) {
	Random randomGen = new Random();
	Vector<Creature> creat = new Vector<Creature>();
	WorldMap.createWorldMap(15,15);
	World wo = new World (15,15);

	// read every file and create a creature of each species, add it to the world, add it to the vector
	// and also display it on the screen
	for(int i = 0; i < s.length; i++){
	    String fileName = s[i];
	    Species spec = new Species(fileName);
	    char name = spec.getName().charAt(0);
	    String color = spec.getColor();
	  
	    // generate a position that is empty
	    int x = randomGen.nextInt(15);
	    int y = randomGen.nextInt(15);
	    Position pos = new Position (x, y);
	    while (!wo.isEmpty(pos)){
		x = randomGen.nextInt(15);
		y = randomGen.nextInt(15);
		pos = new Position (x,y);
	    }
	    
	    int dir = randomGen.nextInt(4);
	    Creature cre = new Creature(spec, wo, pos, dir);
	    wo.set(pos,cre);
	    creat.add(cre);
	    WorldMap.displaySquare(pos,name,dir,color);
	}

	//start the stminator
	Darwin game = new Darwin(creat);
	game.simulate(30000);
	 	
    }

    // makes each creature take one turn for specifed number of rounds
    public void simulate(int rounds) {
	for (int steps = 0; steps < rounds; steps++){
	    oneTurn();
	    WorldMap.pause(50);
	}  	
    }

    //helper method that is called inside simulator and helps simulator take one turn 
    // for each creature
    private void oneTurn(){
	for(int i = 0 ; i < creatures.size(); i++){
	    creatures.get(i).takeOneTurn();
	}
    }

}
