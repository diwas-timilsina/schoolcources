
import structure5.*;

/**
 * This module includes the functions necessary to keep track of the
 * creatures in a two-dimensional world. In order for the design to be
 * general, the interface adopts the following design: <p>
 * 1. The contents have generic type E.  <p>
 * 2. The dimensions of the world array are specified by the client. <p>
 * There are many ways to implement this structure.  HINT: 
 * look at the structure.Matrix class.  You should need to add no more than
 * about ten lines of code to this file.
 *
 * Diwas Timilsina
 * 
 */

public class World<E> { 

    /**
     * This function creates a new world consisting of width columns 
     * and height rows, each of which is numbered beginning at 0. 
     * A newly created world contains no objects. 
     */
    
    // width of the world
    int width;
    
    // height of the world
    int height;

    // world
    private Matrix<E> world; 
    
    // constructor of the world class that takes in the height and width of the
    // world to be created
    public World(int w, int h) {
	world = new Matrix<E>(h,w);
	width = w;
	height = h;
    }

    /**
     * Returns the height of the world.
     */
    public int height() {	
	return height;
    }

    /**
     * Returns the width of the world.
     */
    public int width() {
	return width;
    }

    /**
     * Returns whether pos is in the world or not.
     * @pre  pos is a non-null position.
     * @post returns true if pos is an (x,y) location in 
     *       the bounds of the board.
     */
    boolean inRange(Position pos) {
	int x = pos.getX();
	int y = pos.getY();
	if ((x < width && x >=0) && (y< height && y >= 0)){
	    return true;
	} 
	return false;
    }

    // to check if the position is empty 
   public boolean isEmpty(Position pos){
	if (get(pos) == null){
	    return true;
	} 
	return false;	
    }

    /**
     * Set a position on the board to contain c.
     * @pre  pos is a non-null position on the board.
     */
    public void set(Position pos, E c) {
	int x = pos.getX();
	int y = pos.getY();
	world.set(y,x,c);
    }

    /**
     * Return the contents of a position on the board.
     * @pre  pos is a non-null position on the board.
     */
    public E get(Position pos) {
	if (inRange(pos)){
	    int x = pos.getX();
	    int y = pos.getY();
	    return world.get(y,x);
	}
	return null;
    }



    //code to test the world class
    public static void main(String s[]) {
	World<String> world = new World<String>(15,10);
	Position pos = new Position (5,5);
	world.set(pos,"Inserted");
	System.out.println(world);
	System.out.println(world.inRange(pos));
	System.out.println(world.get(pos));
    }
	
}
