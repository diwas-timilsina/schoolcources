
import structure5.*;
import java.io.*;
import java.util.Scanner;

/**
 * The individual creatures in the world are all representatives of
 * some species class and share certain common characteristics, such
 * as the species name and the program they execute.  Rather than copy
 * this information into each creature, this data can be recorded once
 * as part of the description for a species and then each creature can
 * simply include the appropriate species pointer as part of its
 * internal data structure.
 * <p>
 * 
 * To encapsulate all of the operations operating on a species within
 * this abstraction, this provides a constructor that will read a file
 * containing the name of the creature and its program, as described
 * in the earlier part of this assignment.  To make the folder
 * structure more manageable, the species files for each creature are
 * stored in a subfolder named Creatures.  This, creating the Species
 * for the file Hop.txt will causes the program to read in
 * "Creatures/Hop.txt".
 * 
 * <p>
 *
 * Note: The instruction addresses start at one, not zero.
 *
 *
 * Diwas Timilsina
 * 
 */
public class Species {

    /**
     * Create a species for the given file.
     * @pre fileName exists in the Creature subdirectory
     */
    
    // the total number of instruction
    int total;
    
    // name of the specie
    String name;


    //color of the specie
    String color;
    
    // vector that stores the instruction for the creature
    Vector<Instruction> data;

    // opcode for the instruction
    int opCode;
    
    // address for the opcode 
    int address;
    

    public Species(String fileName)  {
	//take in the file
	Scanner	input =  new Scanner(new FileStream("Creatures" + 
				       java.io.File.separator + 
						    fileName));
	data = new Vector <Instruction>();

	// if no address in the opcode than the address is -1
	address = -1;
	
	// read the name
	if(input.hasNext()){
	    name = input.nextLine();
	}
	
	// read the color
	if(input.hasNext()){
	    color = input.nextLine();
	}
	
	//read the instructions from the file, creat an instructor and then
	// add it to the vector
	
	while(!input.hasNext(".")){
	    
	    total ++;

	    // read in the next token and set the opcode accordingly
	    String str = input.next().toLowerCase();
	    if (str.equals("hop")){
		opCode = 1;
	    }else if (str.equals("left")){
		opCode = 2;    
	    }else if (str.equals("right")){
		opCode = 3;
	    }else if (str.equals("infect")){
		opCode = 4;
	    }else if (str.equals("ifempty")){
		opCode = 5;
	    }else if (str.equals("ifwall")){
		opCode = 6;
	    }else if (str.equals("ifsame")) {
		opCode = 7;
	    }else if(str.equals("ifenemy")) {
		opCode = 8;
	    }else if(str.equals("ifrandom")){
		opCode = 9;
	    }else if(str.equals("go")){
		opCode = 10;
	    } 

	    // read the address if it is there. 
	    if (input.hasNextInt()){
		address = input.nextInt();
	    }
	    
	    Instruction inst = new Instruction (opCode, address);
	    data.add(inst);
	}
    }


    /**
     * Return the name of the species.
     */
    public String getName() {
	return name;
    }

    /**
     * Return the color of the species.
     */
    public String getColor() {
	return color;
    }

    /**
     * Return the number of instructions in the program.
     */
    public int programSize() {
	return total;
    }

    /**
     * Return an instruction from the program.
     * @pre  1 <= i <= programSize().
     * @post returns instruction i of the program.
     */
    public Instruction programStep(int i) {
	return data.get(i-1);
    }

    /**
     * Return a String representation of the program.
     */
    public String programToString() {
	String s = "";
	for (int i = 1; i <= programSize(); i++) {
	    s = s + (i) + ": " + programStep(i) + "\n";
	}
	return s;
    }

    // code to test the species class 
    public static void main(String[] s) {
	Species sp = new Species("Hop.txt");
	System.out.println(sp.getName());
	System.out.println(sp.getColor());

	for (int i = 1; i <= sp.programSize(); i++){
	    System.out.println("first step should be hop: " + sp.programStep(i)); 
	}
	
	

    }

}
   
